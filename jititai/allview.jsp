<%@include file="../header.jsp" %>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>自治体の一覧画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="../CSS/allview.css">
    <link rel="stylesheet" href="jititai_btn.css">
    
</head>
<body>
  <%-- 共有ボタンの設置 --%>
  <%@include file="jititai_btn.jsp" %>

    <h2>${jititai.jititai_name}</h2>

    <div class="content">
        <c:choose>
            <c:when test="${jitigen=='uevent'}">
                <p class="alltitle">イベント一覧（ユーザー)</p> 
                <div class="view">
                <c:forEach var="item" items="${viewuevent}" varStatus="i">
                    <p>【日程${item.event_day}】
                    <a href="Eventlist.action?index=${i.index}&all=ok">
                        ${item.event_title}
                    </a></p>
                </c:forEach>
            </c:when>
            <c:when test="${jitigen=='cevent'}">
                <p>イベント一覧（企業）</p>
                <div class="view">
                <c:forEach var="item" items="${viewcevent}" varStatus="i">
                    <p>【日程${item.event2_day}】
                    <a href="Eventlist2.action?index=${i.index}">
                        ${item.event2_title}
                    </a></p>
                </c:forEach>
            </c:when>
            <c:when test="${jitigen=='mess'}">
                <p class="alltitle">メッセージ一覧</p>
                <div class="view">
                <c:forEach begin="0" end="${size}" varStatus="i">
                    <c:choose>
                        <c:when test="${alljitiread[i.index]=='自治体未読'}">
                            <p>
                                <a href="Jititaimess.action?event2_id=${evelist[i.index].event2_id}&company_id=${comlist[i.index].company_id}">
                                    ${evelist[i.index].event2_title} ${comlist[i.index].company_name}
                                     ${itelist[i.index].item_name}
                                </a>
                                未読あり
                            </p>
                        </c:when>
                        <c:when test="${alljitiread[i.index]!='自治体未読'}">
                            <p>
                                <a href="Jititaimess.action?event2_id=${evelist[i.index].event2_id}&company_id=${comlist[i.index].company_id}">
                                    ${evelist[i.index].event2_title} ${comlist[i.index].company_name}
                                     ${itelist[i.index].item_name}
                                </a>
                            </p>
                        </c:when>
                    </c:choose>
                    
                    
                </c:forEach>
            </c:when>
            <c:when test="${jitigen=='history'}">
                <p class="alltitle">景品交換履歴</p>
                <div class="view">
                <c:forEach begin="0" end="${size}" varStatus="i">
                        <p>${evelist[i.index].event2_title} ${comlist[i.index].company_name}
                             ${itelist[i.index].item_name}
                        </p>
                </c:forEach>
            </c:when>
        </c:choose>
    </div>
    </div>
    <script src="../JS/jquery-3.3.1.min.js"></script>
    <script src="../JS/paginathing.min.js"></script>
    <script src="../JS/user/page.js"></script>
</body>
</html>