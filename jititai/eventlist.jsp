<%@include file="../header.jsp" %>

<!doctype html>
<html>
<head>

   <meta charset="utf-8"/>

   <title>イベント詳細確認画面（ユーザー）</title>

    <link rel="stylesheet" href="../CSS/master.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="jititai_btn.css">
    <link rel="stylesheet" href="../CSS/btn1.css">


</head>

<body>

</div>
<%-- 共有ボタンの設置 --%>
    <%@include file="jititai_btn.jsp"%>

       <h2>イベント詳細</h2>

    </header>

    <div class="content">
        
            
        <text>イベントタイトル</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_title}
        </div>

        <text>イベント内容</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_content}
        </div>

        <text>イベント日時</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_day}
        </div>

        <text>場所</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_place}
        </div>

        <text>参加景品</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_reward}
        </div>

        <text>その他</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_others}
        </div>
    </div>

    <form action="changeevent.jsp" method="post">
        <p class="btn1">
            <button class="btnright">変更</button></p>
    </form>

    <div class="list">
        <c:if test="${eventdate.event_day eq eventtoday}">
            <h4><span class="eventlist_title_left">参加登録</span>
                参加者リスト
                <span class="eventlist_title_right">参加解除</span></h4>
        </c:if>
        <c:if test="${eventdate.event_day != eventtoday}">
            <h4>参加者リスト</h4>
        </c:if>
        <form action="Event_user_select_join.action" method="post" id="user_select">

            

            <c:forEach var="user" items="${user_list}" varStatus="i">
                <c:choose>
                    <%-- イベント当日ならチェックリストを出す --%>
                    <c:when test="${eventdate.event_day eq eventtoday && event_join_flag[i.index] == 0}">
                        <p>
                            <input type="checkbox"  name="today_join" id="${user.user_name}${user.user_id}" value="${user.user_id}">
                            <label for="${user.user_name}${user.user_id}" class="checkbox02">${user.user_name}</label>
                        </p>
                    </c:when>
                    <c:when test="${eventdate.event_day eq eventtoday && event_join_flag[i.index] == 1}">
                        <p>
                            <input type="checkbox"  name="today_notjoin" id="${user.user_name}${user.user_id}" value="${user.user_id}">
                            <label for="${user.user_name}${user.user_id}" class="event_join">${user.user_name}</label>
                        </p>
                    </c:when>
                    <c:when test="${eventdate.event_day eq eventtoday && event_join_flag[i.index] == 2}">
                        <p>
                            通報されたユーザー = ${user.user_name}
                            <input name="report_user" hidden value="${user.user_id}">
                            <% 
                                session.setAttribute("report",2); 
                            %>
                        </p>
                    </c:when>
                    
                    <%-- イベント当日でないなら参加者の名前を表示する --%>
                    <c:otherwise>
                        <p>
                            ${user.user_name}
                            
                        </p>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        
            計${user_list.size()}名
            <c:if test="${eventdate.event_day eq eventtoday}">
                <p class=".btn1">
                    <button name="button" value="join">送信</button>
                </p>
                <c:if test="${report != 2}">
                    <p class=".btn1">
                        <button name="button" value="report">通報</button>
                    </p>
                </c:if>
                <c:if test="${report == 2}">
                    <p class=".btn1">
                        <button name="button" value="notreport">通報取り消し</button>
                    </p>
                </c:if>
            </c:if>    
        </form>
        
    </div>
    
</body>