<%@include file="../header.jsp" %>
<%-- 画面4 --%>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>自治体の一覧画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="jititai_btn.css">
</head>
<body>
    <%@include file="jititai_btn.jsp" %>

       <h2>${jititai.jititai_name}</h2>
    </header>
    <details class="content" open>
        <!-- 参加${count}人
        キャンセル${notcount}人 -->
        <summary>イベント一覧（ユーザー）</summary>

            <c:choose>
                <c:when test="${jitiallevent.size()!=0}">
                    <c:forEach var="event" items="${jitiallevent}" varStatus="i">
                        <p>【日程${event.event_day}】
                        <a href="Eventlist.action?index=${i.index}&all='ng'">
                          ${event.event_title}  
                        </a>                     
                        </p>
                    </c:forEach>
                </c:when>
                <c:when test="${jitiallevent.size()==0}">
                  <p>投稿がありません</p>
                </c:when>
            </c:choose>

        <div class="right">

            <p class="link">
              <a href="Jititaiallveiw.action?jitigen=uevent">もっと見る</a></p>
        </div>
    </details>

    <details class="content" open>
        <summary>イベント一覧（企業）</summary>

        <c:choose>
                <c:when test="${jitiallevent2.size()!=0}">
                    <c:forEach var="event2" items="${jitiallevent2}" varStatus="i">
                        <p>
                            【日程${event2.event2_day}】
                            <a href="Eventlist2.action?index=${i.index}">
                            ${event2.event2_title}
                            </a>
                        </p>
                    </c:forEach>
                </c:when>
                <c:when test="${jitiallevent2.size()==0}">
                  <p>投稿がありません</p>
                </c:when>
        </c:choose>

        <div class="right">
            <p class="link">
              <a href="Jititaiallveiw.action?jitigen=cevent">もっと見る</a></p>
        </div>
    </details>

    <details class="content">
        <summary>メッセージ一覧</summary>

        <c:choose>
                <c:when test="${evelist5j.size()!=0}">
                    <c:forEach begin="0" end="${size5j}" varStatus="i">
                        <c:choose>
                            <c:when test="${jitiread[i.index]=='自治体未読'}">
                                <p>
                                    <a href="Jititaimess.action?event2_id=${evelist5j[i.index].event2_id}&company_id=${comlist5j[i.index].company_id}">
                                        ${evelist5j[i.index].event2_title} ${comlist5j[i.index].company_name}
                                        ${itelist5j[i.index].item_name}
                                    </a>
                                    未読あり
                                </p>
                            </c:when>
                            <c:when test="${jitiread[i.index]!='自治体未読'}">
                                <p>
                                    <a href="Jititaimess.action?event2_id=${evelist5j[i.index].event2_id}&company_id=${comlist5j[i.index].company_id}">
                                        ${evelist5j[i.index].event2_title} ${comlist5j[i.index].company_name}
                                        ${itelist5j[i.index].item_name}
                                    </a>
                                </p>
                            </c:when>
                        </c:choose>
                        
                    </c:forEach>
                </c:when>
                <c:when test="$evelist5j.size()==0}">
                  <p>メッセージがありません</p>
                </c:when>
         </c:choose>

        <div class="right">
            <p class="link">
              <a href="Jititaiallveiw.action?jitigen=mess">もっと見る</a></p>
        </div>
    </details>

    <details class="content">
        <summary>景品交換履歴</summary>

        <c:choose>
                <c:when test="${evelist5j.size()!=0}">
                    <c:forEach begin="0" end="${size5j}" varStatus="i">
                        <a>
                        <p>${comlist5j[i.index].company_name}
                        ${itelist5j[i.index].item_name}
                        </p></a>
                    </c:forEach>
                </c:when>
                <c:when test="${evelist5j.size()==0}">
                  <p>交換履歴がありません</p>
                </c:when>
         </c:choose>

        <div class="right">
            <p class="link">
              <a href="Jititaiallveiw.action?jitigen=history">もっと見る</a></p>
        </div>
    </details>
</body>