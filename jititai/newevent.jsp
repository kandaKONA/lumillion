<%@include file="../header.jsp" %>
<%-- 画面9 --%>
<%@ page import="java.io.*,java.util.*" %>
<%@ page import="javax.servlet.*,java.text.*" %>

<%-- 日付を取得するメソッド --%>
<%!
private String GetDay(){
    Date dNow = new Date( );
    SimpleDateFormat ft = 
    new SimpleDateFormat ("yyyy-MM-dd");
    return ft.format(dNow);
}
   
%>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>イベント（ユーザー）登録画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="gamen9.css">
    <link rel="stylesheet" href="jititai_btn.css">
</head>
<body>
    <%@include file="jititai_btn.jsp" %>
    <%@include file="../error/error.jsp" %>
   
       <h2>イベント登録</h2>
    </header>
    <form action="Newevent.action" method="post">
        <div class="content">
            <text>イベントタイトル</text><br>
            <textarea rows="3" cols="60" name="event_title"></textarea><br>
            <text>イベント日時</text><br>
            <%-- 現在の日付を取得しその日以前を選択できないようにする --%>
            <input type="date" name="event_day" min="<%= GetDay()%>"><br>

            <text>場所</text><br>
            <%-- 都道府県のセレクトボックス nameはpref --%>
            <select name="pref" required>
            <%@include file="../search/toduhuken.jsp" %>
            <textarea rows="3" cols="60" name="event_place"
            required></textarea><br>
            <text>内容</text><br>
            <textarea rows="6" cols="60" name="event_content"
            required></textarea><br>
            <text>景品</text><br>
            <textarea rows="3" cols="60" name="event_reward"
            required></textarea><br>
            <text>参加人数上限</text><br>
            <input type="tel" name="event_number" required>
            <br>
            <text>その他</text><br>
            <textarea rows="9" cols="60" name="event_others"
            ></textarea><br>
        
        </div>
        <p class="btn1">
            <button type="submit">投稿</button></p>
    </form>
    
</body>