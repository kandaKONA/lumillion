<%@include file="../header.jsp" %>
<!doctype html>
<html>
<head>

   <meta charset="utf-8"/>

   <title>イベント詳細確認画面（企業）</title>

    <link rel="stylesheet" href="../CSS/master.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="jititai_btn.css">
    <style>
        .btn1 {
            font-size: 33px;
            width: 150px;
            padding: 30px;
            margin: 0 auto;
            
        }

        .btnright{
            right: 63px;
        }
    
    </style>


</head>

<body>

</div>
<%-- 共有ボタンの設置 --%>
    <%@include file="jititai_btn.jsp"%>

       <h2>イベント詳細</h2>

    </header>

    <div class="content">

        <text>イベントタイトル</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event2date.event2_title}
        </div>

        <text>イベント内容</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event2date.event2_text}
        </div>

        <text>イベント日時</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event2date.event2_day}
        </div>
    </div>


    <form action="changeevent2.jsp" method="post">
        <p class="btn1">
        <button class="btnright">変更</button></p>
    </form>

    <div class="list">
        <p>メッセージ一覧</p>
        <c:forEach begin="0" end="${size5}" varStatus="i">
            <c:choose>
                <c:when test="${jitiread[i.index]=='自治体未読'}">
                    <p>
                        <a href="Jititaimess.action?event2_id=${event2date.event2_id}&company_id=${ecomlist[i.index].company_id}">
                
                            ${ecomlist[i.index].company_name}
                            ${eitelist[i.index].item_name}
                        </a>
                        未読あり
                    </p>
                </c:when>
                <c:when test="${jitiread[i.index]!='自治体未読'}">
                    <p>
                        <a href="Jititaimess.action?event2_id=${event2date.event2_id}&company_id=${ecomlist[i.index].company_id}">
                
                            ${ecomlist[i.index].company_name}
                            ${eitelist[i.index].item_name}
                        </a>
                    </p>
                </c:when>
            </c:choose>
            
        </c:forEach>
    </div>
    
</body>