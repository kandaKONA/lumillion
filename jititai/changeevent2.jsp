<%@include file="../header.jsp" %>
<%-- 画面9 --%>
<%@ page import="java.io.*,java.util.*" %>
<%@ page import="javax.servlet.*,java.text.*" %>

<%-- 日付を取得するメソッド --%>
<%!
private String GetDay(){
    Date dNow = new Date( );
    SimpleDateFormat ft = 
    new SimpleDateFormat ("yyyy-MM-dd");
    return ft.format(dNow);
}
   
%>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>景品募集変更画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="gamen9.css">
    <link rel="stylesheet" href="jititai_btn.css">
</head>
<body>
    <%@include file="jititai_btn.jsp" %>
    <%@include file="../error/error.jsp" %>
   
       <h2>イベント変更(景品募集)</h2>
    </header>
    <form action="ChangeEvent2.action" method="post">
        <div class="content">
            <text>イベントタイトル</text><br>
            <textarea rows="3" cols="60" name="event2_title"
            required>${event2date.event2_title}</textarea><br>
            <text>イベント日時</text><br>
            <%-- 現在の日付を取得しその日以前を選択できないようにする --%>
            <input type="date" name="event2_day" min="<%= GetDay()%>" 
            value="${event2date.event2_day}" required><br>
            <text>内容</text><br>
            <textarea rows="6" cols="60" name="event2_text" 
            required>${event2date.event2_text}</textarea><br>

            <text>メッセージ</text><br>
            <textarea name="textarea" cols="60" rows="5" 
            placeholder="景品をいただいてる企業に向けて送る変更内容をお書きください"></textarea>
        </div>

        <br><br>

        

        <p class="btn1">
            <button type="submit">変更</button></p>
    </form>
    
</body>