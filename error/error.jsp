<c:choose>
    <%-- ログイン失敗時 --%>
    <c:when test="${error=='login'}">
        <div id="login">
            ユーザー名又はパスワードが間違っています
        </div>
        
    </c:when>

    <%-- 登録失敗時 --%>
    <c:when test="${error=='new'}">
        <div id="new">
            必須項目は空白では登録できません
        </div>
    </c:when>

    <%-- 同じ電話番号登録失敗時 --%>
    <c:when test="${error=='tel'}">
        <div id="new">
            既に登録されている電話番号です
        </div>
    </c:when>

    <%-- イベント参加登録失敗時 --%>
    <c:when test="${error=='join'}">
        <div id="join">
            イベントの参加に失敗しました。
            既にイベントに参加していないか確認してください
        </div>
    </c:when>

    <%-- 変更失敗時 --%>
    <c:when test="${error=='change'}">
        <div id="change">
            必須項目は空白に変更できません
        </div>

    </c:when>

    <%-- 景品未選択時 company/eventitem.jsp--%>
    <c:when test="${error=='item'}">
        <div id="item">
            景品が選択されていません
            リストボックスから送る景品を選択してください
        </div>
    </c:when>

    <%-- メッセージ失敗時 --%>
    <c:when test="${error=='mess'}">
        <div id="mess">
            メッセージを入力してください
        </div>
    </c:when>

    <%-- 上限を超えた時 --%>
    <c:when test="${error=='limit'}">
        <div id="limit">
            人数が上限に達しています
        </div>
    </c:when>

    <%-- 同じ日のイベントに参加した時 --%>
    <c:when test="${error=='same'}">
        <div id="limit">
            同じ日付のイベントに参加しています
        </div>
    </c:when>

    <%-- 通報回数が上限を超えた時 --%>
    <c:when test="${error=='ng'}">
        <div id="limit">
            通報回数が上限に達しているためイベントに参加できません
        </div>
    </c:when>
</c:choose>
<% 
    session.setAttribute("error","null"); 
%>