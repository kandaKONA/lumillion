package user;

import bean.*;
import login.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;

public class UsertopAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        User use = (User) session.getAttribute("user");

        
        //ログインアクションを使って情報更新

        UserloginAction uaction = new UserloginAction();
        List<Event> allevent = uaction.allevent();
        session.setAttribute("allevent", allevent);
        List<Event> alljoin = uaction.alljoin(use);
        session.setAttribute("alljoin", alljoin);
        List<Event> allmess = uaction.allmess(use);
        session.setAttribute("allmess", allmess);
        List<String> read_check = uaction.allmessread(use);
        session.setAttribute("allmessread", read_check);
        //正常なページ遷移
        response.sendRedirect("user.jsp");

        return "";
    }
    
}
