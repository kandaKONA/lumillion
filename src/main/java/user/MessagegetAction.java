package user;

import bean.*;
import dao.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;

public class MessagegetAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        User use = (User) session.getAttribute("user");

        User_message mess = new User_message();
        mess.setUserfk_id(use.getUser_id());

        User_messageDAO dao = new User_messageDAO();
        List<User_message> user_message = dao.slecttalk(mess);
        if(user_message != null){
            //全てのeventの情報をセッションに保存
            session.setAttribute("user_message", user_message);

            response.sendRedirect("eventlist.jsp");
        }
        
        return "";
    }
    
}
