package user;

import bean.*;
import dao.*;
import login.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;

public class EventjoinAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        Event event = (Event) session.getAttribute("event");
        User use = (User) session.getAttribute("user");
         //beanのEvent_listに保存
         Event_list list_in = new Event_list();
         list_in.setEvent_id(event.getevent_id());
         list_in.setUser_id(use.getUser_id());
         
         //Event_listDAOを使いDBに追加
         boolean flag = false;
         Event_listDAO dao = new Event_listDAO();
         EventDAO dao3 = new EventDAO();
         List<Event> event2 = (List<Event>) session.getAttribute("alljoin");
         int count = dao.count(event.getevent_id());
         Event day = dao3.selectday2(event.getevent_id());
         String today = day.getevent_day();
         for (int i = 0; i < event2.size(); i++) {
            if (event2.get(i).getevent_day().equals(today)) {
                flag = true;
                break;
            }
        }

        if (use.getUser_ngcount() >= 3) {
            //登録失敗ページに遷移
            session.setAttribute("error", "ng");
            response.sendRedirect("user.jsp");
            return " ";
        }

        if (count >= event.getevent_number()) {
            //登録失敗ページに遷移
            session.setAttribute("error", "limit");
            response.sendRedirect("event_join.jsp");
            return " ";
        }else if (flag) {
            session.setAttribute("error", "same");
            response.sendRedirect("event_join.jsp");
            return " ";
        } else {
            int insert = dao.insert(list_in);
            if(insert == 0){
                //登録失敗ページに遷移
                session.setAttribute("error", "join");
                response.sendRedirect("user.jsp");
                return " ";
            }
            String text = event.getevent_title() +  "ご参加いただきありがとうございます。\n";
            User_message mess = new User_message();
            mess.setEvent3_id(event.getevent_id());
            mess.setJititai3_id(event.getjititai4_id());
            mess.setUserfk_id(use.getUser_id());
            mess.setUmess_text(text);
            User_messageDAO dao2 = new User_messageDAO();
            insert = dao2.insert(mess);

            if(insert != 0){
                List<User_message> message = dao2.slecttalk(mess);
                //ログインアクションを使って情報更新
                UserloginAction uaction = new UserloginAction();
                List<Event> alljoin = uaction.alljoin(use);
                session.setAttribute("alljoin", alljoin);
                List<Event> allmess = uaction.allmess(use);
                session.setAttribute("allmess", allmess);
                List<String> read_check = uaction.allmessread(use);
                session.setAttribute("allmessread", read_check);
                //検索した結果をmessageの情報をセッションに保存
                session.setAttribute("user_message", message);
                //正常なページ遷移
                response.sendRedirect("user.jsp");
            }
            
        }

        return "";
    }
    
}
