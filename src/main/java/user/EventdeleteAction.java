package user;

import bean.*;
import dao.*;
import login.*;
import tool.Action;

import java.nio.channels.AlreadyConnectedException;
import java.util.List;
import javax.servlet.http.*;

public class EventdeleteAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        Event event = (Event) session.getAttribute("eventdate");
        User use = (User) session.getAttribute("user");
         //beanのEvent_listに保存
         Event_list list_in = new Event_list();
         list_in.setEvent_id(event.getevent_id());
         list_in.setUser_id(use.getUser_id());
         System.out.println(event.getevent_id() + "と"+ use.getUser_id());
         
            Event_listDAO dao2 = new Event_listDAO();
            int delete = dao2.delete(list_in);

            if(delete != 0){
                User_messageDAO dao3 = new User_messageDAO();
                int message = dao3.delete(list_in);
                //ログインアクションを使って情報更新
                UserloginAction uaction = new UserloginAction();
                List<Event> alljoin = uaction.alljoin(use);
                session.setAttribute("alljoin", alljoin);
                List<Event> allmess = uaction.allmess(use);
                session.setAttribute("allmess", allmess);
                //検索した結果をmessageの情報をセッションに保存
                session.setAttribute("user_message", message);
                //正常なページ遷移
                response.sendRedirect("user.jsp");
            }
            

        return "";
    }
}
