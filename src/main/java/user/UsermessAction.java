package user;

import bean.*;
import dao.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;

public class UsermessAction  extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        User use = (User) session.getAttribute("user");
        int event_id = Integer.parseInt(request.getParameter("event_id"));
           
        User_message mess = new User_message();
        mess.setUserfk_id(use.getUser_id());
        mess.setEvent3_id(event_id);

        //既読変更
        Event_listDAO eventdao = new Event_listDAO();
        eventdao.read_change(event_id, use.getUser_id(), "既読");

        User_messageDAO dao = new User_messageDAO();
        List<User_message> user_message = dao.limitslecttalk(mess);
        if(user_message != null){
            //全てのeventの情報をセッションに保存
            session.setAttribute("user_message", user_message);

            response.sendRedirect("user_message.jsp");
        }
        
        return "";
    }
    
}
