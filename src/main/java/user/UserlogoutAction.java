package user;

import tool.Action;
import javax.servlet.http.*;

public class UserlogoutAction extends Action {
	public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
		//セッションの確認
        HttpSession session=request.getSession();
		//既にログインしているか
        if (session.getAttribute("user")!=null) {
			//セッションの破棄
			session.removeAttribute("user");
			//session.invalidate();
			//正常なページ遷移
			response.sendRedirect("../top/Top.action");
		}
		//エラーの時のページ遷移
        return "../top/top.jsp";
    }
    
}
