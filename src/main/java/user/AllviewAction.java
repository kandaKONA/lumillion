package user;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import java.util.Collections;

import java.util.ArrayList;
import java.util.List;

public class AllviewAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();
        
        String gen = request.getParameter("gen");

        //送られてきたgenでどの情報を引き出すか決める
        //イベント
        if (gen.equals("event")) {
            EventDAO dao = new EventDAO();
            List<Event> viewevent = dao.limitslect();
            if(viewevent != null){
                //全てのeventの情報をセッションに保存
                session.setAttribute("viewevent", viewevent);
                session.setAttribute("gen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }

        }else if(gen.equals("join")){
            //イベント参加一覧
            User use = (User) session.getAttribute("user");
            Event_listDAO dao = new Event_listDAO();
            
            List<Event_list> viewjoin = dao.selectjoin(use.getUser_id());
            if (viewjoin != null) {
                EventDAO dao2 = new EventDAO();
                List<Event> viewevent = new ArrayList<Event>();
                Event event = new Event();
                for (int i = 0; i < viewjoin.size(); i++) {
                    event = dao2.selectday(viewjoin.get(i).getEvent_id());
                    if (event != null) {
                        viewevent.add(event);
                    }
                }
                //ソート
                Collections.sort(viewevent, new Event_sort());

                //全てのeventの情報をセッションに保存
                session.setAttribute("viewjoin", viewevent);
                session.setAttribute("gen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }
            

        }else if(gen.equals("mess")){
            //メッセージ一覧
            User use = (User) session.getAttribute("user");
            Event_listDAO dao = new Event_listDAO();
            
            List<Event_list> viewjoin = dao.selectjoin(use.getUser_id());
            if (viewjoin != null) {
                EventDAO dao2 = new EventDAO();
                List<Event> viewevent = new ArrayList<Event>();
                Event event = new Event();
                for (int i = 0; i < viewjoin.size(); i++) {
                    event = dao2.select(viewjoin.get(i).getEvent_id());
                    if (event != null) {
                        viewevent.add(event);
                    }
                }
                //全てのeventの情報をセッションに保存
                session.setAttribute("viewmess", viewevent);
                session.setAttribute("gen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }

        }else if(gen.equals("joined")){
            //イベント参加一覧
            User use = (User) session.getAttribute("user");
            Event_listDAO dao = new Event_listDAO();
            
            List<Event_list> viewjoin = dao.selectjoined(use.getUser_id());
            if (viewjoin != null) {
                EventDAO dao2 = new EventDAO();
                List<Event> viewevent = new ArrayList<Event>();
                Event event = new Event();
                for (int i = 0; i < viewjoin.size(); i++) {
                    event = dao2.select(viewjoin.get(i).getEvent_id());
                    if (event != null) {
                        viewevent.add(event);
                    }
                }
                //全てのeventの情報をセッションに保存
                session.setAttribute("viewjoined", viewevent);
                session.setAttribute("gen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }
        }

        

        return "";
    }

}