package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.InputStream;
import javax.imageio.ImageIO;

public class ItemDAO extends DAO{

     //景品の追加
     public int insert(Item item) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into item values(null,?, ?, ?, ?)"
            );
            st.setInt(1, item.getCompany3_id());
            st.setString(2, item.getItem_name());
            st.setString(3, item.getItem_text());
            st.setBytes(4, item.getItem_image());
    
            //追加された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //追加された列数を返却
            return line;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }
    
    //リストの参照
    public Item select(int item_id) throws Exception{
        Item item = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item where item_id=? "
        );
        st.setInt(1, item_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_listに結果を保存して返却
        while(rs.next()){
            item = new Item();
            item.setItem_id(rs.getInt("item_id"));
            item.setCompany3_id(rs.getInt("company3_id"));
            item.setItem_name(rs.getString("item_name"));
            item.setItem_text(rs.getString("item_text"));
            item.setItem_image(rs.getBytes("item_image"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return item;
    }

    //リストの参照
    public BufferedImage selectimage(int item_id) throws Exception{
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item where item_id=? "
        );
        st.setInt(1, item_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
        BufferedInputStream bis;
        //item_listに結果を保存して返却
        while(rs.next()){
            InputStream is = rs.getBinaryStream("item_image");
			bis = new BufferedInputStream(is);
			return ImageIO.read(bis);
        }
        //DBの接続切断
        st.close();
        con.close();
        return null;
        
    }

    public List<Item> allselect() throws Exception{
        //配列の作成
        List<Item> list=new ArrayList<Item>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item "
        );
        
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_listに結果を保存して返却
        while(rs.next()){
            Item item = new Item();
            item.setItem_id(rs.getInt("item_id"));
            item.setCompany3_id(rs.getInt("company3_id"));
            item.setItem_name(rs.getString("item_name"));
            item.setItem_text(rs.getString("item_text"));
            item.setItem_image(rs.getBytes("item_image"));
            //リストに追加
            list.add(item);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
    }

    public List<Item> allselect(int company_id) throws Exception{
        //配列の作成
        List<Item> list=new ArrayList<Item>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item where company3_id = ?"
        );
        st.setInt(1, company_id);
        
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_listに結果を保存して返却
        while(rs.next()){
            Item item = new Item();
            item.setItem_id(rs.getInt("item_id"));
            item.setCompany3_id(rs.getInt("company3_id"));
            item.setItem_name(rs.getString("item_name"));
            item.setItem_text(rs.getString("item_text"));
            item.setItem_image(rs.getBytes("item_image"));
            //リストに追加
            list.add(item);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
    }
    
    //内容の変更
    public int update(int item_id, String item_name, String item_text, byte[] item_image){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE item SET item_name=?, item_text=?, item_image=? WHERE item_id=?"
            );
            st.setString(1, item_name);
            st.setInt(4, item_id);
            st.setBytes(3, item_image);
            st.setString(2, item_text);
            //変更された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }
}
