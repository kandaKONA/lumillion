package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

public class JititaiDAO extends DAO{

    //自治体の追加
    public int insert(Jititai jititai) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into jititai values(null, ?, ?, ?, ?)"
            );
            st.setString(1, jititai.getJititai_name());
            st.setString(2, jititai.getJititai_addres());
            st.setString(3, jititai.getJititai_mailaddres());
            st.setString(4, jititai.getJititai_pass());
    
            //追加された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //追加された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
    
    //自治体の参照
    public Jititai select(String jititai_name, String jititai_pass) throws Exception{
        Jititai jititai = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from jititai where jititai_name=? and jititai_pass=?"
        );
        st.setString(1, jititai_name);
        st.setString(2, jititai_pass);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //jititaiに結果を保存して返却
        while(rs.next()){
            jititai = new Jititai();
            jititai.setJititai_id(rs.getInt("jititai_id"));
            jititai.setJititai_name(rs.getString("jititai_name"));
            jititai.setJititai_addres(rs.getString("jititai_addres"));
            jititai.setJititai_mailaddres(rs.getString("jititai_mailaddres"));
            jititai.setJititai_pass(rs.getString("jititai_pass"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return jititai;
    }

    public Jititai select(int jititai_id) throws Exception{
        Jititai jititai = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from jititai where jititai_id=?"
        );
        st.setInt(1, jititai_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //jititaiに結果を保存して返却
        while(rs.next()){
            jititai = new Jititai();
            jititai.setJititai_id(rs.getInt("jititai_id"));
            jititai.setJititai_name(rs.getString("jititai_name"));
            jititai.setJititai_addres(rs.getString("jititai_addres"));
            jititai.setJititai_mailaddres(rs.getString("jititai_mailaddres"));
            jititai.setJititai_pass(rs.getString("jititai_pass"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return jititai;
    }

    public List<Jititai> allselect() throws Exception{
        //配列の作成
        List<Jititai> list=new ArrayList<Jititai>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from jititai "
        );
        
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //jititai_listに結果を保存して返却
        while(rs.next()){
            Jititai jititai = new Jititai();
            jititai.setJititai_id(rs.getInt("jititai_id"));
            jititai.setJititai_name(rs.getString("jititai_name"));
            jititai.setJititai_addres(rs.getString("jititai_addres"));
            jititai.setJititai_mailaddres(rs.getString("jititai_mailaddres"));
            //リストに追加
            list.add(jititai);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
    }

    public List<Jititai> getpass(String name) throws Exception{
        //配列の作成
        List<Jititai> list=new ArrayList<Jititai>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from jititai where jititai_name=?"
        );
        st.setString(1, name);
        
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //jititai_listに結果を保存して返却
        while(rs.next()){
            Jititai jititai = new Jititai();
            jititai.setJititai_name(rs.getString("jititai_name"));
            jititai.setJititai_pass(rs.getString("jititai_pass"));
            //リストに追加
            list.add(jititai);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
    }

    
    //内容の変更
    public int update(String jititai_name, String jititai_addres, String jititai_mailaddres, int jititai_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE jititai SET jititai_name=?, jititai_addres=?, jititai_mailaddres=? WHERE jititai_id=?"
            );
            st.setString(1, jititai_name);
            st.setString(2, jititai_addres);
            st.setString(3, jititai_mailaddres);
            st.setInt(4, jititai_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}
