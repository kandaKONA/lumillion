package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

public class Event2DAO extends DAO{

    //今日の日付を取得し返却する
    public String getday(){
        Date dNow = new Date( );
        SimpleDateFormat ft = 
        new SimpleDateFormat ("yyyy-MM-dd");
        return ft.format(dNow);
    }
    
    //イベントーの追加
    public int insert(Event2 event2) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into event2 values(null, ?, ?, ?, ?, ?)"
            );
            st.setInt(1, event2.getjititai5_id());
            st.setString(2, event2.getevent2_title());
            st.setString(3, event2.getevent2_day());
            st.setString(4, event2.getevent2_text());
            st.setString(5, event2.getevent2_place());

            //追加された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //追加された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    //イベントの参照
    public Event2 select(int event2_id) throws Exception{
        Event2 event2 = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event2 where event2_id=?"
        );
        st.setInt(1, event2_id);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //event2に結果を保存して返却
        while(rs.next()){
            event2 = new Event2();
            event2.setevent2_id(rs.getInt("event2_id"));
            event2.setjititai5_id(rs.getInt("jititai5_id"));
            event2.setevent2_title(rs.getString("event2_title"));
            event2.setevent2_day(rs.getString("event2_day"));
            event2.setevent2_text(rs.getString("event2_text"));
            event2.setevent2_place(rs.getString("event2_place"));
        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return event2;
    }

    //自治体のメイン画面に表示するイベント５件
    public List<Event2> slectall(int jiti) throws Exception{
        //配列の作成
        List<Event2> list=new ArrayList<Event2>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event2 WHERE event2_day > ? AND jititai5_id = ? ORDER BY event2_id DESC LIMIT 5"
        );
        st.setString(1, getday());
        st.setInt(2, jiti);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //event2に結果を保存して返却
        while(rs.next()){
            Event2 event2 = new Event2();
            event2.setevent2_id(rs.getInt("event2_id"));
            event2.setjititai5_id(rs.getInt("jititai5_id"));
            event2.setevent2_title(rs.getString("event2_title"));
            event2.setevent2_day(rs.getString("event2_day"));
            event2.setevent2_text(rs.getString("event2_text"));
            event2.setevent2_place(rs.getString("event2_place"));
            //リストに追加
            list.add(event2);
        }
        //DBの接続切断
        st.close();
        con.close();


        return list;
    }

    //ユーザー画面に表示されるイベント
    public List<Event2> slectall() throws Exception{
        //配列の作成
        List<Event2> list=new ArrayList<Event2>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event2 WHERE event2_day > ? ORDER BY event2_id DESC LIMIT 5"
        );
        st.setString(1, getday());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //event2に結果を保存して返却
        while(rs.next()){
            Event2 event2 = new Event2();
            event2.setevent2_id(rs.getInt("event2_id"));
            event2.setjititai5_id(rs.getInt("jititai5_id"));
            event2.setevent2_title(rs.getString("event2_title"));
            event2.setevent2_day(rs.getString("event2_day"));
            event2.setevent2_text(rs.getString("event2_text"));
            event2.setevent2_place(rs.getString("event2_place"));
            //リストに追加
            list.add(event2);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //キーワード検索未入力5件
    public List<Event2> slectall(String sort) throws Exception{
        //配列の作成
        List<Event2> list=new ArrayList<Event2>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st;
        String sql = "";
        if (sort.equals("Newevent")) {
            //イベント降順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_id DESC LIMIT 5";
        }else if(sort.equals("Oldevent")){
            //イベント昇順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_id ASC LIMIT 5";
        }else if(sort.equals("Newdayevent")){
            //日付降順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_day ASC LIMIT 5";
        }else if(sort.equals("Olddayevent")){
            //日付昇順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_day DESC LIMIT 5";
        }
        st=con.prepareStatement(sql);
        st.setString(1, getday());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //event2に結果を保存して返却
        while(rs.next()){
            Event2 event2 = new Event2();
            event2.setevent2_id(rs.getInt("event2_id"));
            event2.setjititai5_id(rs.getInt("jititai5_id"));
            event2.setevent2_title(rs.getString("event2_title"));
            event2.setevent2_day(rs.getString("event2_day"));
            event2.setevent2_text(rs.getString("event2_text"));
            event2.setevent2_place(rs.getString("event2_place"));
            //リストに追加
            list.add(event2);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //今日以降開催予定の全てのイベントを格納する
    public List<Event2> limitslect() throws Exception{
        //配列の作成
        List<Event2> list=new ArrayList<Event2>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event2 WHERE event2_day > ? ORDER BY event2_id DESC"
        );
        st.setString(1, getday());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //event2に結果を保存して返却
        while(rs.next()){
            Event2 event2 = new Event2();
            event2.setevent2_id(rs.getInt("event2_id"));
            event2.setjititai5_id(rs.getInt("jititai5_id"));
            event2.setevent2_title(rs.getString("event2_title"));
            event2.setevent2_day(rs.getString("event2_day"));
            event2.setevent2_text(rs.getString("event2_text"));
            event2.setevent2_place(rs.getString("event2_place"));
            //リストに追加
            list.add(event2);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //自治体のイベントもっと見るで呼び出される
    public List<Event2> limitslect(int jiti) throws Exception{
        //配列の作成
        List<Event2> list=new ArrayList<Event2>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event2 WHERE event2_day > ? AND jititai5_id = ? ORDER BY event2_id DESC"
        );
        st.setString(1, getday());
        st.setInt(2, jiti);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //event2に結果を保存して返却
        while(rs.next()){
            Event2 event2 = new Event2();
            event2.setevent2_id(rs.getInt("event2_id"));
            event2.setjititai5_id(rs.getInt("jititai5_id"));
            event2.setevent2_title(rs.getString("event2_title"));
            event2.setevent2_day(rs.getString("event2_day"));
            event2.setevent2_text(rs.getString("event2_text"));
            event2.setevent2_place(rs.getString("event2_place"));
            //リストに追加
            list.add(event2);
        }
        //DBの接続切断
        st.close();
        con.close();


        return list;
    }

    //キーワード検索未入力
    public List<Event2> limitslect(String sort) throws Exception{
        //配列の作成
        List<Event2> list=new ArrayList<Event2>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st;
        String sql = "";
        if (sort.equals("Newevent")) {
            //イベント降順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_id DESC";
        }else if(sort.equals("Oldevent")){
            //イベント昇順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_id ASC";
        }else if(sort.equals("Newdayevent")){
            //日付降順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_day ASC";
        }else if(sort.equals("Olddayevent")){
            //日付昇順
            sql = "select * from event2 WHERE event2_day > ? " +
            "ORDER BY event2_day DESC";
        }
        st=con.prepareStatement(sql);
        st.setString(1, getday());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //event2に結果を保存して返却
        while(rs.next()){
            Event2 event2 = new Event2();
            event2.setevent2_id(rs.getInt("event2_id"));
            event2.setjititai5_id(rs.getInt("jititai5_id"));
            event2.setevent2_title(rs.getString("event2_title"));
            event2.setevent2_day(rs.getString("event2_day"));
            event2.setevent2_text(rs.getString("event2_text"));
            event2.setevent2_place(rs.getString("event2_place"));
            //リストに追加
            list.add(event2);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //地域検索
    public List<Event2> place_search(String search, String sort){
        try {
            //配列の作成
            List<Event2> list=new ArrayList<Event2>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";
            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_id DESC";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_id ASC";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_day ASC";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_day DESC";
            }
            st=con.prepareStatement(sql);
            st.setString(1, getday());
            st.setString(2, search + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //event2に結果を保存して返却
            while(rs.next()){
                Event2 event2 = new Event2();
                event2.setevent2_id(rs.getInt("event2_id"));
                event2.setjititai5_id(rs.getInt("jititai5_id"));
                event2.setevent2_title(rs.getString("event2_title"));
                event2.setevent2_day(rs.getString("event2_day"));
                event2.setevent2_text(rs.getString("event2_text"));
                event2.setevent2_place(rs.getString("event2_place"));
                //リストに追加
                list.add(event2);
            }
            //DBの接続切断
            st.close();
            con.close();

            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //地域検索5件
    public List<Event2> place_searchlimit(String search, String sort){
        try {
            //配列の作成
            List<Event2> list=new ArrayList<Event2>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";
            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_id DESC LIMIT 5";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_id ASC LIMIT 5";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_day ASC LIMIT 5";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? ORDER BY event2_day DESC LIMIT 5";
            }
            st=con.prepareStatement(sql);
            st.setString(1, getday());
            st.setString(2, search + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //event2に結果を保存して返却
            while(rs.next()){
                Event2 event2 = new Event2();
                event2.setevent2_id(rs.getInt("event2_id"));
                event2.setjititai5_id(rs.getInt("jititai5_id"));
                event2.setevent2_title(rs.getString("event2_title"));
                event2.setevent2_day(rs.getString("event2_day"));
                event2.setevent2_text(rs.getString("event2_text"));
                event2.setevent2_place(rs.getString("event2_place"));
                //リストに追加
                list.add(event2);
            }
            //DBの接続切断
            st.close();
            con.close();

            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //キーワード検索
    public List<Event2> keyword_search(String search, String sort){
        try {
            //配列の作成
            List<Event2> list=new ArrayList<Event2>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";
            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_id DESC";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_id ASC";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_day ASC";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_day DESC";
            }
            st=con.prepareStatement(sql);
            st.setString(1, getday());
            st.setString(2, "%" + search + "%");
            st.setString(3, "%" + search + "%");
            st.setString(4, "%" + search + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //event2に結果を保存して返却
            while(rs.next()){
                Event2 event2 = new Event2();
                event2.setevent2_id(rs.getInt("event2_id"));
                event2.setjititai5_id(rs.getInt("jititai5_id"));
                event2.setevent2_title(rs.getString("event2_title"));
                event2.setevent2_day(rs.getString("event2_day"));
                event2.setevent2_text(rs.getString("event2_text"));
                event2.setevent2_place(rs.getString("event2_place"));
                //リストに追加
                list.add(event2);
            }
            //DBの接続切断
            st.close();
            con.close();

            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //キーワード検索5件
    public List<Event2> keyword_searchlimit(String search, String sort){
        try {
            //配列の作成
            List<Event2> list=new ArrayList<Event2>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";
            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_id DESC LIMIT 5";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_id ASC LIMIT 5";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_day ASC LIMIT 5";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ? ORDER BY event2_day DESC LIMIT 5";
            }
            st=con.prepareStatement(sql);
            st.setString(1, getday());
            st.setString(2, "%" + search + "%");
            st.setString(3, "%" + search + "%");
            st.setString(4, "%" + search + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //event2に結果を保存して返却
            while(rs.next()){
                Event2 event2 = new Event2();
                event2.setevent2_id(rs.getInt("event2_id"));
                event2.setjititai5_id(rs.getInt("jititai5_id"));
                event2.setevent2_title(rs.getString("event2_title"));
                event2.setevent2_day(rs.getString("event2_day"));
                event2.setevent2_text(rs.getString("event2_text"));
                event2.setevent2_place(rs.getString("event2_place"));
                //リストに追加
                list.add(event2);
            }
            //DBの接続切断
            st.close();
            con.close();

            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //地域且つキーワード検索
    public List<Event2> place_keyword_search(String place,String keyword, String sort){
        try {
            //配列の作成
            List<Event2> list=new ArrayList<Event2>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";
            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_id DESC";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_id ASC";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_day ASC";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_day DESC";
            }
            st=con.prepareStatement(sql);
            st.setString(1, getday());
            st.setString(2, place + "%");  //前方一致
            st.setString(3, "%" + keyword + "%");  //部分一致
            st.setString(4, "%" + keyword + "%");
            st.setString(5, "%" + keyword + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //event2に結果を保存して返却
            while(rs.next()){
                Event2 event2 = new Event2();
                event2.setevent2_id(rs.getInt("event2_id"));
                event2.setjititai5_id(rs.getInt("jititai5_id"));
                event2.setevent2_title(rs.getString("event2_title"));
                event2.setevent2_day(rs.getString("event2_day"));
                event2.setevent2_text(rs.getString("event2_text"));
                event2.setevent2_place(rs.getString("event2_place"));
                //リストに追加
                list.add(event2);
            }
            //DBの接続切断
            st.close();
            con.close();

            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //地域且つキーワード検索５件
    public List<Event2> place_keyword_searchlimit(String place,String keyword, String sort){
        try {
            //配列の作成
            List<Event2> list=new ArrayList<Event2>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";
            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_id DESC LIMIT 5";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_id ASC LIMIT 5";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_day ASC LIMIT 5";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event2 WHERE event2_day > ? " +
                "AND event2_place LIKE ? AND event2_place LIKE ? OR event2_text LIKE ?" +
                " OR event2_title LIKE ORDER BY event2_day DESC LIMIT 5";
            }
            st=con.prepareStatement(sql);
            st.setString(1, getday());
            st.setString(2, place + "%");  //前方一致
            st.setString(3, "%" + keyword + "%");  //部分一致
            st.setString(4, "%" + keyword + "%");
            st.setString(5, "%" + keyword + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //event2に結果を保存して返却
            while(rs.next()){
                Event2 event2 = new Event2();
                event2.setevent2_id(rs.getInt("event2_id"));
                event2.setjititai5_id(rs.getInt("jititai5_id"));
                event2.setevent2_title(rs.getString("event2_title"));
                event2.setevent2_day(rs.getString("event2_day"));
                event2.setevent2_text(rs.getString("event2_text"));
                event2.setevent2_place(rs.getString("event2_place"));
                //リストに追加
                list.add(event2);
            }
            //DBの接続切断
            st.close();
            con.close();

            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //内容の変更
    public int update(String event2_title, String event2_day, String event2_text, int event2_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE event2 SET event2_title=?, event2_day=?, event2_text=? WHERE event2_id=?"
            );
            st.setString(1, event2_title);
            st.setString(2, event2_day);
            st.setString(3, event2_text);
            st.setInt(4, event2_id);
            //変更された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    //内容の削除
    public int delete(int event2_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "DELETE event2 WHERE event2_id=?"
            );
            st.setInt(1, event2_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}
