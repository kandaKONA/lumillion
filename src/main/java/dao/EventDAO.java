package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

public class EventDAO extends DAO{

    //今日の日付を取得し返却する
    public String getday(){
        Date dNow = new Date( );
        SimpleDateFormat ft = 
        new SimpleDateFormat ("yyyy-MM-dd");
        return ft.format(dNow);
    }

    //イベントーの追加
    public int insert(Event event) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into event values(null, ?, ?, ?, ?, ?, ?, ?,?)"
            );
            st.setInt(1, event.getjititai4_id());
            st.setString(2, event.getevent_title());
            st.setString(3, event.getevent_day());
            st.setString(4, event.getevent_place());
            st.setString(5, event.getevent_content());
            st.setString(6, event.getevent_reward());
            st.setInt(7, event.getevent_number());
            st.setString(8, event.getevent_others());

            //追加された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //追加された列数を返却
            return line;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

    //イベントの参照
    public Event selectday(int event_id) throws Exception{
        Event event = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event where event_id=? and event_day >= ?"
        );
        st.setInt(1, event_id);
        st.setString(2, getday());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            event = new Event();
            event.setevent_id(rs.getInt("event_id"));
            event.setjititai4_id(rs.getInt("jititai4_id"));
            event.setevent_title(rs.getString("event_title"));
            event.setevent_day(rs.getString("event_day"));
            event.setevent_place(rs.getString("event_place"));
            event.setevent_content(rs.getString("event_content"));
            event.setevent_reward(rs.getString("event_reward"));
            event.setevent_number(rs.getInt("event_number"));
            event.setevent_others(rs.getString("event_others"));
        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return event;
    }

    //イベントの参照
    public Event selectday2(int event_id) throws Exception{
        Event event = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event where event_id=?"
        );
        st.setInt(1, event_id);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            event = new Event();
            event.setevent_day(rs.getString("event_day"));
        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return event;
    }

    //イベントの参照日付
    public Event select(int event_id) throws Exception{
        Event event = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event where event_id=? ORDER BY event_day DESC"
        );
        st.setInt(1, event_id);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            event = new Event();
            event.setevent_id(rs.getInt("event_id"));
            event.setjititai4_id(rs.getInt("jititai4_id"));
            event.setevent_title(rs.getString("event_title"));
            event.setevent_day(rs.getString("event_day"));
            event.setevent_place(rs.getString("event_place"));
            event.setevent_content(rs.getString("event_content"));
            event.setevent_reward(rs.getString("event_reward"));
            event.setevent_number(rs.getInt("event_number"));
            event.setevent_others(rs.getString("event_others"));
        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return event;
    }

    //イベントテーブルの日付が今日以降の最新情報を5件取得してList<Event>に
    //格納する
    public List<Event> slectall(int jiti) throws Exception{
        //配列の作成
        List<Event> list=new ArrayList<Event>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event WHERE event_day >= ? AND jititai4_id = ? ORDER BY event_id DESC LIMIT 5"
        );
        st.setString(1, getday());
        st.setInt(2, jiti);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            Event event = new Event();
            event.setevent_id(rs.getInt("event_id"));
            event.setjititai4_id(rs.getInt("jititai4_id"));
            event.setevent_title(rs.getString("event_title"));
            event.setevent_day(rs.getString("event_day"));
            event.setevent_place(rs.getString("event_place"));
            event.setevent_content(rs.getString("event_content"));
            event.setevent_reward(rs.getString("event_reward"));
            event.setevent_number(rs.getInt("event_number"));
            event.setevent_others(rs.getString("event_others"));
            //リストに追加
            list.add(event);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //ユーザー画面に表示されるイベント
    public List<Event> slectall() throws Exception{
        //配列の作成
        List<Event> list=new ArrayList<Event>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event WHERE event_day >= ? ORDER BY event_id DESC LIMIT 5"
        );
        st.setString(1, getday());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            Event event = new Event();
            event.setevent_id(rs.getInt("event_id"));
            event.setjititai4_id(rs.getInt("jititai4_id"));
            event.setevent_title(rs.getString("event_title"));
            event.setevent_day(rs.getString("event_day"));
            event.setevent_place(rs.getString("event_place"));
            event.setevent_content(rs.getString("event_content"));
            event.setevent_reward(rs.getString("event_reward"));
            event.setevent_number(rs.getInt("event_number"));
            event.setevent_others(rs.getString("event_others"));
            //リストに追加
            list.add(event);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //キーワード検索未入力
    public List<Event> slectall(String sort) throws Exception{
        //配列の作成
        List<Event> list=new ArrayList<Event>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st;
        String sql = "";
        if (sort.equals("Newevent")) {
            //イベント降順
            sql = "select * from event WHERE event_day >= ? " + 
            "ORDER BY event_id DESC LIMIT 5";
        }else if(sort.equals("Oldevent")){
            //イベント昇順
            sql = "select * from event WHERE event_day >= ? " + 
            "ORDER BY event_id ASC LIMIT 5";
        }else if(sort.equals("Newdayevent")){
            //日付降順
            sql = "select * from event WHERE event_day >= ? " + 
            "ORDER BY event_day ASC LIMIT 5";
        }else if(sort.equals("Olddayevent")){
            //日付昇順
            sql = "select * from event WHERE event_day >= ? " + 
            "ORDER BY event_day DESC LIMIT 5";
        }
        st=con.prepareStatement(sql);

        st.setString(1, getday());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            Event event = new Event();
            event.setevent_id(rs.getInt("event_id"));
            event.setjititai4_id(rs.getInt("jititai4_id"));
            event.setevent_title(rs.getString("event_title"));
            event.setevent_day(rs.getString("event_day"));
            event.setevent_place(rs.getString("event_place"));
            event.setevent_content(rs.getString("event_content"));
            event.setevent_reward(rs.getString("event_reward"));
            event.setevent_number(rs.getInt("event_number"));
            event.setevent_others(rs.getString("event_others"));
            //リストに追加
            list.add(event);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //自治体のイベントもっと見るで呼び出される
    public List<Event> limitslect(int jiti){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "select * from event WHERE jititai4_id = ? ORDER BY event_day DESC"
            );
            st.setInt(1, jiti);

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
        
    }

    //ユーザーのイベントもっと見るで呼び出される
    public List<Event> limitslect(){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "select * from event WHERE event_day >= ? ORDER BY event_id DESC"
            );
            st.setString(1, getday());

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
        
    }

    //キーワード検索時何も入力されてなかったら呼び出される
    public List<Event> limitslect(String sort){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";
            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event WHERE event_day >= ? " + 
                "ORDER BY event_id DESC";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event WHERE event_day >= ? " + 
                "ORDER BY event_id ASC";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event WHERE event_day >= ? ORDER BY event_day ASC";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event WHERE event_day >= ? ORDER BY event_day DESC";
            }
            st=con.prepareStatement(sql);
            st.setString(1, getday());

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
        
    }

    //地域検索
    public List<Event> place_search(String search, String sort){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";

            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_id DESC";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_id ASC";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_day ASC";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_day DESC";
            }

            st = con.prepareStatement(sql);
            st.setString(1, search + "%");  //前方一致

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //キーワード検索
    public List<Event> keyword_search(String search, String sort){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";

            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id DESC";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id ASC";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day ASC";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day DESC";
            }

            st = con.prepareStatement(sql);
            st.setString(1, "%" + search + "%");  //部分一致
            st.setString(2, "%" + search + "%");
            st.setString(3, "%" + search + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //地域且つキーワード検索
    public List<Event> place_keyword_search(String place,String keyword, String sort){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";

            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id DESC";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id ASC";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day DESC";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day ASC";
            }

            st = con.prepareStatement(sql);
            st.setString(1, place + "%");  //前方一致
            st.setString(2, "%" + keyword + "%");  //部分一致
            st.setString(3, "%" + keyword + "%");
            st.setString(4, "%" + keyword + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //地域検索５件
    public List<Event> place_searchlimit(String search, String sort){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";

            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_id DESC LIMIT 5";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_id ASC LIMIT 5";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_day ASC LIMIT 5";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "ORDER BY event_day DESC LIMIT 5";
            }

            st = con.prepareStatement(sql);
            st.setString(1, search + "%");  //前方一致

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //キーワード検索5件
    public List<Event> keyword_searchlimit(String search, String sort){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";

            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id DESC" + 
                " LIMIT 5";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id ASC" + 
                " LIMIT 5";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day ASC" + 
                " LIMIT 5";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event WHERE event_place LIKE ? " + 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day DESC" + 
                " LIMIT 5";
            }

            st = con.prepareStatement(sql);
            st.setString(1, "%" + search + "%");  //部分一致
            st.setString(2, "%" + search + "%");
            st.setString(3, "%" + search + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //地域且つキーワード検索５件
    public List<Event> place_keyword_searchlimit(String place,String keyword, String sort){
        try {
            //配列の作成
            List<Event> list=new ArrayList<Event>();
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st;
            String sql = "";

            if (sort.equals("Newevent")) {
                //イベント降順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id DESC" + 
                " LIMIT 5";
            }else if(sort.equals("Oldevent")){
                //イベント昇順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_id ASC" + 
                " LIMIT 5";
            }else if(sort.equals("Newdayevent")){
                //日付降順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day DESC" + 
                " LIMIT 5";
            }else if(sort.equals("Olddayevent")){
                //日付昇順
                sql = "select * from event WHERE event_place LIKE ? AND event_place LIKE ? "+ 
                "OR event_content LIKE ? OR event_title LIKE ? ORDER BY event_day ASC" + 
                " LIMIT 5";
            }

            st = con.prepareStatement(sql);
            st.setString(1, place + "%");  //前方一致
            st.setString(2, "%" + keyword + "%");  //部分一致
            st.setString(3, "%" + keyword + "%");
            st.setString(4, "%" + keyword + "%");

            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();

            //eventに結果を保存して返却
            while(rs.next()){
                Event event = new Event();
                event.setevent_id(rs.getInt("event_id"));
                event.setjititai4_id(rs.getInt("jititai4_id"));
                event.setevent_title(rs.getString("event_title"));
                event.setevent_day(rs.getString("event_day"));
                event.setevent_place(rs.getString("event_place"));
                event.setevent_content(rs.getString("event_content"));
                event.setevent_reward(rs.getString("event_reward"));
                event.setevent_number(rs.getInt("event_number"));
                event.setevent_others(rs.getString("event_others"));
                //リストに追加
                list.add(event);
            }
            //DBの接続切断
            st.close();
            con.close();
            return list;
        } catch (Exception e) {
            return null;
        }
    }

    //内容の変更
    public int update(String event_title, String event_day, String event_place, String event_content, String event_reward, String event_number, String event_others, int event_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE event SET event_title=?, event_day=?, event_place=?, event_content=?, event_reward=?, event_number=?, event_others=? WHERE event_id=?"
            );
            st.setString(1, event_title);
            st.setString(2, event_day);
            st.setString(3, event_place);
            st.setString(4, event_content);
            st.setString(5, event_reward);
            st.setString(6, event_number);
            st.setString(7, event_others);
            st.setInt(8, event_id);
            //変更された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    //内容の削除
    public int delete(int event_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "DELETE event WHERE event_id=?"
            );
            st.setInt(1, event_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}
