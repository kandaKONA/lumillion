package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

public class MessageDAO extends DAO{

    //企業・自治体のメッセージの追加
    public int insert(Message message) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into message values(null, ?, ?, ?, now(), ? ,?)"
            );
            st.setInt(1, message.getEvent2_id());
            st.setInt(2, message.getCompany2_id());
            st.setInt(3, message.getJititai2_id());
            st.setString(4, message.getMess_text());
            st.setInt(5, message.getFlag());
    
            //追加された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //追加された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
    
    //メッセージの参照
    public Message select(int mess_id) throws Exception{
        Message message = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from message where mess_id=?"
        );
        st.setInt(1, mess_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //messageに結果を保存して返却
        while(rs.next()){
            message = new Message();
            message.setMess_id(rs.getInt("mess_id"));
            message.setEvent2_id(rs.getInt("event2_id"));
            message.setCompany2_id(rs.getInt("company2_id"));
            message.setJititai2_id(rs.getInt("jititai2_id"));
            message.setMess_day(rs.getTimestamp("mess_day"));
            message.setMess_text(rs.getString("mess_text"));
            message.setFlag(rs.getInt("flag"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return message;
    }

    //イベントテーブル全表示List<Message>にイベントがすべて入る
    public List<Message> slectall() throws Exception{
        //配列の作成
        List<Message> list=new ArrayList<Message>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from message"
        );

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            Message message = new Message();
            message.setMess_id(rs.getInt("mess_id"));
            message.setEvent2_id(rs.getInt("event2_id"));
            message.setCompany2_id(rs.getInt("company2_id"));
            message.setJititai2_id(rs.getInt("jititai2_id"));
            message.setMess_day(rs.getTimestamp("mess_day"));
            message.setMess_text(rs.getString("mess_text"));
            message.setFlag(rs.getInt("flag"));
            //リストに追加
            list.add(message);
        }
        //DBの接続切断
        st.close();
        con.close();


        return list;
    }

    //特定の会社のメッセージ一覧
    public List<Message> slectallcom(int company_id) throws Exception{
        //配列の作成
        List<Message> list=new ArrayList<Message>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from message where company2_id = ?"
        );
        st.setInt(1, company_id);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            Message message = new Message();
            message.setMess_id(rs.getInt("mess_id"));
            message.setEvent2_id(rs.getInt("event2_id"));
            message.setCompany2_id(rs.getInt("company2_id"));
            message.setJititai2_id(rs.getInt("jititai2_id"));
            message.setMess_day(rs.getTimestamp("mess_day"));
            message.setMess_text(rs.getString("mess_text"));
            message.setFlag(rs.getInt("flag"));
            //リストに追加
            list.add(message);
        }
        //DBの接続切断
        st.close();
        con.close();


        return list;
    }

    //メッセージテーブルの最新内容がList<Message>に5つのメッセージが入る
    public List<Message> slecttalk(Message message) throws Exception{
        //配列の作成
        List<Message> list=new ArrayList<Message>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from message" +
        "WHERE jititai2_id=? and event2_id=? and company2_id=?" + 
        "ORDER BY mess_id DESC LIMIT 5"
        );
        st.setInt(1, message.getJititai2_id());
        st.setInt(2, message.getEvent2_id());
        st.setInt(3, message.getCompany2_id());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            Message mess = new Message();
            mess.setMess_id(rs.getInt("mess_id"));
            mess.setEvent2_id(rs.getInt("event2_id"));
            mess.setCompany2_id(rs.getInt("company2_id"));
            mess.setJititai2_id(rs.getInt("jititai2_id"));
            mess.setMess_day(rs.getTimestamp("mess_day"));
            mess.setMess_text(rs.getString("mess_text"));
            mess.setFlag(rs.getInt("flag"));
            //リストに追加
            list.add(mess);
        }
        //DBの接続切断
        st.close();
        con.close();


        return list;
    }

    //指定したmess_idより前の情報をList<Message>にlimit数入る
    public List<Message> limitslecttalk(Message message) throws Exception{
        //配列の作成
        List<Message> list=new ArrayList<Message>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from message WHERE jititai2_id=? and event2_id=? and company2_id=? ORDER BY mess_id"
        );
        st.setInt(1, message.getJititai2_id());
        st.setInt(2, message.getEvent2_id());
        st.setInt(3, message.getCompany2_id()); 

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            Message mess = new Message();
            mess.setMess_id(rs.getInt("mess_id"));
            mess.setEvent2_id(rs.getInt("event2_id"));
            mess.setCompany2_id(rs.getInt("company2_id"));
            mess.setJititai2_id(rs.getInt("jititai2_id"));
            mess.setMess_day(rs.getTimestamp("mess_day"));
            mess.setMess_text(rs.getString("mess_text"));
            mess.setFlag(rs.getInt("flag"));
            //リストに追加
            list.add(mess);
        }
        //DBの接続切断
        st.close();
        con.close();


        return list;
    }

    //内容の変更
    public int update(int mess_id, String mess_text){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE item SET item_text=? WHERE mess_id=?"
            );
            st.setInt(2, mess_id);
            st.setString(1, mess_text);
            //変更された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
    
    //内容の削除
    public int delete(int mess_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "DELETE message WHERE mess_id=?"
            );
            st.setInt(1, mess_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}
