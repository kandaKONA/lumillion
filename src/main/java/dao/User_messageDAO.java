package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

public class User_messageDAO extends DAO{

    //企業・自治体のメッセージの追加
    public int insert(User_message user_message) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into user_message values(null, ?, ?, ?, now(), ?)"
            );
            st.setInt(1, user_message.getEvent3_id());
            st.setInt(2, user_message.getUserfk_id());
            st.setInt(3, user_message.getJititai3_id());
            st.setString(4, user_message.getUmess_text());
    
            //追加された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //追加された列数を返却
            return line;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }
    
    //メッセージの参照
    public User_message select(int umess_id) throws Exception{
        User_message user_message = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user_message where umess_id=?"
        );
        st.setInt(1, umess_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //user_messageに結果を保存して返却
        while(rs.next()){
            user_message = new User_message();
            user_message.setUmess_id(rs.getInt("umess_id"));
            user_message.setEvent3_id(rs.getInt("event3_id"));
            user_message.setUserfk_id(rs.getInt("userfk_id"));
            user_message.setJititai3_id(rs.getInt("jititai3_id"));
            user_message.setUmess_day(rs.getTimestamp("umess_day"));
            user_message.setUmess_text(rs.getString("umess_text"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return user_message;
    }

    //イベントテーブル全表示List<Message>にイベントがすべて入る
    public List<User_message> slectall() throws Exception{
        //配列の作成
        List<User_message> list=new ArrayList<User_message>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user_message"
        );

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            User_message user_message = new User_message();
            user_message.setUmess_id(rs.getInt("umess_id"));
            user_message.setEvent3_id(rs.getInt("event3_id"));
            user_message.setUserfk_id(rs.getInt("userfk_id"));
            user_message.setJititai3_id(rs.getInt("jititai3_id"));
            user_message.setUmess_day(rs.getTimestamp("umess_day"));
            user_message.setUmess_text(rs.getString("umess_text"));
            //リストに追加
            list.add(user_message);
        }
        //DBの接続切断
        st.close();
        con.close();


        return list;
    }

    //ユーザーメッセージテーブルの最新内容がList<User_Message>に5つのメッセージが入る
    public List<User_message> slecttalk(User_message user_message) throws Exception{
        //配列の作成
        List<User_message> list=new ArrayList<User_message>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user_message WHERE userfk_id=? ORDER BY umess_id DESC LIMIT 5"
        );
        st.setInt(1, user_message.getUserfk_id());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            User_message mess = new User_message();
            mess.setUmess_id(rs.getInt("umess_id"));
            mess.setEvent3_id(rs.getInt("event3_id"));
            mess.setUserfk_id(rs.getInt("userfk_id"));
            mess.setJititai3_id(rs.getInt("jititai3_id"));
            mess.setUmess_day(rs.getTimestamp("umess_day"));
            mess.setUmess_text(rs.getString("umess_text"));
            //リストに追加
            list.add(mess);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //指定したumess_idより前の情報をList<Message>にlimit数入る
    public List<User_message> limitslecttalk(User_message user_message) throws Exception{
        //配列の作成
        List<User_message> list=new ArrayList<User_message>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user_message WHERE userfk_id=? and event3_id=? ORDER BY umess_id"
        );
        st.setInt(1, user_message.getUserfk_id());
        st.setInt(2, user_message.getEvent3_id());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //eventに結果を保存して返却
        while(rs.next()){
            User_message mess = new User_message();
            mess.setUmess_id(rs.getInt("umess_id"));
            mess.setEvent3_id(rs.getInt("event3_id"));
            mess.setUserfk_id(rs.getInt("userfk_id"));
            mess.setJititai3_id(rs.getInt("jititai3_id"));
            mess.setUmess_day(rs.getTimestamp("umess_day"));
            mess.setUmess_text(rs.getString("umess_text"));
            //リストに追加
            list.add(mess);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //内容の変更
    public int update(int umess_id, String umess_text){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE item SET umess_text=? WHERE umess_id=?"
            );
            st.setInt(2, umess_id);
            st.setString(1, umess_text);
            //変更された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
    
    //内容の削除
    public int delete(Event_list list2){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "DELETE FROM user_message WHERE event3_id=? and userfk_id = ?"
            );
            st.setInt(1, list2.getEvent_id());
            st.setInt(2, list2.getUser_id());
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}
