package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

public class Item_historyDAO extends DAO{

    //景品の交換履歴の追加
    public int insert(Item_history item_history) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into item_history values(0, ?, ?, ?, ?, now(), ?)"
            );
            st.setInt(1, item_history.getEvent2_id());
            st.setInt(2, item_history.getItem_id());
            st.setInt(3, item_history.getJititai_id());
            st.setInt(4, item_history.getCompany_id());
            st.setString(5, item_history.getRead_check());
    
            //追加された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //追加された列数を返却
            return line;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }
    
    //履歴の参照
    public Item_history select(int history_id) throws Exception{
        Item_history item_history = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history where history_id=?"
        );
        st.setInt(1, history_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            item_history = new Item_history();
            item_history.setHistory_id(rs.getInt("history_id"));
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setCreat_time(rs.getTimestamp("creat_time"));
            item_history.setRead_check(rs.getString("read_check"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return item_history;
    }

    //履歴の参照
    public Item_history select_read(Item_history read) throws Exception{
        Item_history item_history = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history where company_id=? AND jititai_id=? AND event2_id=?"
        );
        st.setInt(1, read.getCompany_id());
        st.setInt(2, read.getJititai_id());
        st.setInt(3, read.getEvent2_id());
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            item_history = new Item_history();
            item_history.setHistory_id(rs.getInt("history_id"));
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setCreat_time(rs.getTimestamp("creat_time"));
            item_history.setRead_check(rs.getString("read_check"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return item_history;
    }

    //企業が行った交換履歴の最新情報を５件表示する
    public List<Item_history> selectcomlimit(int company_id) throws Exception{
        //配列の作成
        List<Item_history> list=new ArrayList<Item_history>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history" + 
        "WHERE company_id = ?" + 
        "ORDER BY history_id DESC LIMIT 5"
        );
        st.setInt(1, company_id);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            Item_history item_history = new Item_history();
            item_history.setHistory_id(rs.getInt("history_id"));
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setCreat_time(rs.getTimestamp("creat_time"));
            item_history.setRead_check(rs.getString("read_check"));
            //リストに追加
            list.add(item_history);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //自治体のやり取りリスト参照
    public List<Item_history> selectalljiti(Item_history item) throws Exception{
        //配列の作成
        List<Item_history> list=new ArrayList<Item_history>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history WHERE jititai_id = ? ORDER BY creat_time DESC"
        );
        st.setInt(1, item.getJititai_id());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            Item_history item_history = new Item_history();
            item_history.setHistory_id(rs.getInt("history_id"));
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setCreat_time(rs.getTimestamp("creat_time"));
            item_history.setRead_check(rs.getString("read_check"));
            //リストに追加
            list.add(item_history);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //企業のやり取りリスト参照
    public List<Item_history> selectallcom(Item_history item) throws Exception{
        //配列の作成
        List<Item_history> list=new ArrayList<Item_history>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history WHERE company_id = ? ORDER BY creat_time DESC"
        );
        st.setInt(1, item.getCompany_id());

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            Item_history item_history = new Item_history();
            item_history.setHistory_id(rs.getInt("history_id"));
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setCreat_time(rs.getTimestamp("creat_time"));
            item_history.setRead_check(rs.getString("read_check"));
            //リストに追加
            list.add(item_history);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //イベントから企業検索
    public List<Item_history> selectevent(int event) throws Exception{
        //配列の作成
        List<Item_history> list=new ArrayList<Item_history>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history WHERE event2_id = ? group by company_id"
        );
        st.setInt(1, event);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            Item_history item_history = new Item_history();
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setRead_check(rs.getString("read_check"));
            //リストに追加
            list.add(item_history);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //企業のやり取りリスト参照
    public List<Item_history> selectallevent(int item) throws Exception{
        //配列の作成
        List<Item_history> list=new ArrayList<Item_history>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history WHERE event2_id = ? ORDER BY creat_time DESC"
        );
        st.setInt(1, item);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            Item_history item_history = new Item_history();
            item_history.setHistory_id(rs.getInt("history_id"));
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setCreat_time(rs.getTimestamp("creat_time"));
            item_history.setRead_check(rs.getString("read_check"));
            //リストに追加
            list.add(item_history);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }

    //自治体が行った交換履歴から情報を5件表示する
    public List<Item_history> limitselectalljiti(int jititai_id, int history_id, int limit) throws Exception{
        //配列の作成
        List<Item_history> list=new ArrayList<Item_history>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from item_history" + 
        "WHERE jititai_id = ? AND history_id < ?" + 
        "ORDER BY creat_time DESC LIMIT ?"
        );
        st.setInt(1, jititai_id);
        st.setInt(2, history_id);
        st.setInt(3, limit);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //item_historyに結果を保存して返却
        while(rs.next()){
            Item_history item_history = new Item_history();
            item_history.setHistory_id(rs.getInt("history_id"));
            item_history.setEvent2_id(rs.getInt("event2_id"));
            item_history.setItem_id(rs.getInt("item_id"));
            item_history.setJititai_id(rs.getInt("jititai_id"));
            item_history.setCompany_id(rs.getInt("company_id"));
            item_history.setCreat_time(rs.getTimestamp("creat_time"));
            item_history.setRead_check(rs.getString("read_check"));
            //リストに追加
            list.add(item_history);
        }
        //DBの接続切断
        st.close();
        con.close();

        return list;
    }
    
    //内容の削除
    public int delete(int history_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "DELETE item_history WHERE event_id=?"
            );
            st.setInt(1, history_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    public int change_read(Item_history read_check){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE item_history SET read_check = ?, creat_time = now() WHERE company_id = ? AND jititai_id=? AND event2_id=?"
            );
            st.setString(1, read_check.getRead_check());
            st.setInt(2, read_check.getCompany_id());
            st.setInt(3, read_check.getJititai_id());
            st.setInt(4, read_check.getEvent2_id());
            
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}
