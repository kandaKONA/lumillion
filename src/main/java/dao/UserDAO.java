package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

public class UserDAO extends DAO {
    
    //ユーザーの追加
    public int insert(User user) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into user values(null, ?, ?, ?, 0)"
            );
            st.setString(1, user.getUser_name());
            st.setString(2, user.getUser_pass());
            st.setString(3, user.getUser_tel());

            //追加された列数を取得
            int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //追加された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    //ユーザーの参照
    public User select(String user_name, String user_pass) throws Exception{
        User user = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user where user_name=? and user_pass=?"
        );
        st.setString(1, user_name);
        st.setString(2, user_pass);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //userに結果を保存して返却
        while(rs.next()){
            user = new User();
            user.setUser_id(rs.getInt("user_id"));
            user.setUser_name(rs.getString("user_name"));
            user.setUser_pass(rs.getString("user_pass"));
            user.setUser_tel(rs.getString("user_tel"));
            user.setUser_ngcount(rs.getInt("user_ngcount"));

        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return user;
    }

    //ユーザーの参照
    public User select(int user_id) throws Exception{
        User user = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user where user_id=?"
        );
        st.setInt(1, user_id);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //userに結果を保存して返却
        while(rs.next()){
            user = new User();
            user.setUser_id(rs.getInt("user_id"));
            user.setUser_name(rs.getString("user_name"));
            user.setUser_pass(rs.getString("user_pass"));
            user.setUser_tel(rs.getString("user_tel"));
            user.setUser_ngcount(rs.getInt("user_ngcount"));
        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return user;
    }

    //名前検索をしてそのパスワードを取得
    public List<User> getpass(String name) throws Exception{
        List<User> list = new ArrayList<User>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user where user_name=?"
        );
        st.setString(1, name);

        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //userに結果を保存して返却
        while(rs.next()){
            User user = new User();
            user.setUser_pass(rs.getString("user_pass"));
            //リストに追加
            list.add(user);
        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return list;
    }

    //電話番号を取得
    public List<User> gettel() throws Exception{
        List<User> list = new ArrayList<User>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from user"
        );
        
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();

        //userに結果を保存して返却
        while(rs.next()){
            User user = new User();
            user.setUser_tel(rs.getString("user_tel"));
            //リストに追加
            list.add(user);
        }
        //DBの接続切断
        st.close();
        con.close();

        //結果の返却
        return list;
    }

    //内容の変更
    public int update(String user_name, String user_tel, int user_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
	        PreparedStatement st=con.prepareStatement(
	        "UPDATE user SET user_name=? user_tel=? WHERE user_id=?"
	        );
	        st.setString(1, user_name);
            st.setString(2, user_tel);
	        st.setInt(3, user_id);
            //変更された列数を取得
	        int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    public int ngcount(int user_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
	        PreparedStatement st=con.prepareStatement(
	        "UPDATE user SET user_ngcount=(user_ngcount + 1) WHERE user_id=?"
	        );
	        st.setInt(1, user_id);
            //変更された列数を取得
	        int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    public int not_ngcount(int user_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
	        PreparedStatement st=con.prepareStatement(
	        "UPDATE user SET user_ngcount=(user_ngcount - 1) WHERE user_id=?"
	        );
	        st.setInt(1, user_id);
            //変更された列数を取得
	        int line=st.executeUpdate();

            //DBの接続切断
            st.close();
            con.close();

            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}
