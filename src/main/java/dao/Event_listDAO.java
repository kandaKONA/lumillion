package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Event_listDAO extends DAO{

    //イベントに参加するユーザーの追加
    public int insert(Event_list event_list) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into event_list values(?, ?, now(),0,'未読')"
            );
            st.setInt(1, event_list.getEvent_id());
            st.setInt(2, event_list.getUser_id());
            
    
            //追加された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //追加された列数を返却
            return line;
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }
    
    //リストの参照
    public Event_list select(int event_id, int user_id) throws Exception{
        Event_list event_list = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event_list where event_id=? and user_id=?"
        );
        st.setInt(1, event_id);
        st.setInt(2, user_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //event_listに結果を保存して返却
        while(rs.next()){
            event_list = new Event_list();
            event_list.setEvent_id(rs.getInt("event_id"));
            event_list.setUser_id(rs.getInt("user_id"));
            event_list.setCreat_time(rs.getTimestamp("creat_time"));
            event_list.setJoin_flag(rs.getInt("join_flag"));
            event_list.setRead_check(rs.getString("read_check"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return event_list;
    }

    //リストの参照
    public List<Event_list> select(int event_id){
        try {
            //配列の作成
        List<Event_list> list=new ArrayList<Event_list>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event_list where event_id=?"
        );
        st.setInt(1, event_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //event_listに結果を保存して返却
        while(rs.next()){
            Event_list event_list = new Event_list();
            event_list.setEvent_id(rs.getInt("event_id"));
            event_list.setUser_id(rs.getInt("user_id"));
            event_list.setCreat_time(rs.getTimestamp("creat_time"));
            event_list.setJoin_flag(rs.getInt("join_flag"));
            event_list.setRead_check(rs.getString("read_check"));
            list.add(event_list);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    

    //リストの参照
    public List<Event_list> selectjoined(int user_id){
        try {
            //配列の作成
        List<Event_list> list=new ArrayList<Event_list>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event_list where user_id=? and join_flag=1 ORDER BY event_id DESC"
        );
        st.setInt(1, user_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //event_listに結果を保存して返却
        while(rs.next()){
            Event_list event_list = new Event_list();
            event_list.setEvent_id(rs.getInt("event_id"));
            event_list.setUser_id(rs.getInt("user_id"));
            event_list.setCreat_time(rs.getTimestamp("creat_time"));
            event_list.setJoin_flag(rs.getInt("join_flag"));
            event_list.setRead_check(rs.getString("read_check"));
            list.add(event_list);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
        
    }

    public List<Event_list> selectjoin(int user_id){
        try {
            //配列の作成
        List<Event_list> list=new ArrayList<Event_list>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from event_list where user_id=? ORDER BY event_id DESC"
        );
        st.setInt(1, user_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //event_listに結果を保存して返却
        while(rs.next()){
            Event_list event_list = new Event_list();
            event_list.setEvent_id(rs.getInt("event_id"));
            event_list.setUser_id(rs.getInt("user_id"));
            event_list.setCreat_time(rs.getTimestamp("creat_time"));
            event_list.setJoin_flag(rs.getInt("join_flag"));
            event_list.setRead_check(rs.getString("read_check"));
            list.add(event_list);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
        
    }
    
    //内容の削除
    public int delete(int event_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "DELETE event_list WHERE event_id=?"
            );
            st.setInt(1, event_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    //参加の削除
    public int delete(Event_list list){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "DELETE FROM event_list WHERE event_id=? and user_id=?"
            );
            st.setInt(1, list.getEvent_id());
            st.setInt(2, list.getUser_id());
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }

    //カウント
    public int count(int event_id) throws Exception{
        int count = 0;
       //DBに接続
       Connection con=getConnection();
       //SQL文の作成
       PreparedStatement st=con.prepareStatement(
       "SELECT COUNT(*) AS user_count FROM event_list WHERE event_id = ?"
       );
       st.setInt(1, event_id);

       //SQL文の実行結果をrsに保存
       ResultSet rs=st.executeQuery();
    
       //event_listに結果を保存して返却
       while(rs.next()){
            count = rs.getInt("user_count");
       }

       //DBの接続切断
       st.close();
       con.close();

       return count;
    }

    //フラグの書き換え
    public int flagchange(int event_id, int user_id, int flag) throws Exception{
        
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE event_list SET join_flag = ? WHERE event_id=? and user_id=?"
            );
            st.setInt(1, flag);
            st.setInt(2, event_id);
            st.setInt(3, user_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
    }

    //通報取り消し
    public int notreport(int event_id) throws Exception{
        
        //DBに接続
        Connection con=getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "UPDATE event_list SET join_flag = 0 WHERE event_id=? and join_flag=2"
        );
        st.setInt(1, event_id);
        //変更された列数を取得
        int line=st.executeUpdate();

        //DBの接続切断
        st.close();
        con.close();

        //変更された列数を返却
        return line;
    }

    //既読変更
    public int read_change(int event_id, int user_id, String read_check) throws Exception{
        
        //DBに接続
        Connection con=getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "UPDATE event_list SET read_check = ?, creat_time = now() WHERE event_id=? and user_id=?"
        );
        st.setString(1, read_check);
        st.setInt(2, event_id);
        st.setInt(3, user_id);
        //変更された列数を取得
        int line=st.executeUpdate();

        //DBの接続切断
        st.close();
        con.close();

        //変更された列数を返却
        return line;
    }

    //未読変更
    public int read_midoku_change(int event_id, String read_check) throws Exception{
        
        //DBに接続
        Connection con=getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "UPDATE event_list SET read_check = ?, creat_time = now() WHERE event_id=?"
        );
        st.setString(1, read_check);
        st.setInt(2, event_id);
        //変更された列数を取得
        int line=st.executeUpdate();

        //DBの接続切断
        st.close();
        con.close();

        //変更された列数を返却
        return line;
    }
}
