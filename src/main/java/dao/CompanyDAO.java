package dao;

import bean.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.ArrayList;

public class CompanyDAO extends DAO{

    //企業の追加
    public int insert(Company company) throws Exception{
        try {
            //DBに接続
            Connection con = getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "insert into company values(?, ?, ?, ?, ?)"
            );
            st.setInt(1, company.getCompany_id());
            st.setString(2, company.getCompany_name());
            st.setString(3, company.getCompany_addres());
            st.setString(4, company.getCompany_mailaddres());
            st.setString(5, company.getCompany_pass());
    
            //追加された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //追加された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
    
    //企業の参照
    public Company select(String company_name, String company_pass){
        Company company = null;
        //DBに接続
        Connection con;
        try {
            con = getConnection();
        
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "select * from company where company_name=? and company_pass=?"
            );
            st.setString(1, company_name);
            st.setString(2, company_pass);
            
            //SQL文の実行結果をrsに保存
            ResultSet rs=st.executeQuery();
            
            //companyに結果を保存して返却
            while(rs.next()){
                company = new Company();
                company.setCompany_id(rs.getInt("company_id"));
                company.setCompany_name(rs.getString("company_name"));
                company.setCompany_addres(rs.getString("company_addres"));
                company.setCompany_mailaddres(rs.getString("company_mailaddres"));
                company.setCompany_pass(rs.getString("company_pass"));
            }
            //DBの接続切断
            st.close();
            con.close();
    
            //結果の返却
            return company;
        } catch (Exception e) {
            return null;
        }
    }
    
    //企業の参照
    public Company select(int company_id) throws Exception{
        Company company = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from company where company_id=?"
        );
        st.setInt(1, company_id);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //companyに結果を保存して返却
        while(rs.next()){
            company = new Company();
            company.setCompany_id(rs.getInt("company_id"));
            company.setCompany_name(rs.getString("company_name"));
            company.setCompany_addres(rs.getString("company_addres"));
            company.setCompany_mailaddres(rs.getString("company_mailaddres"));
            company.setCompany_pass(rs.getString("company_pass"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return company;
    }

    //idの最大値を取得
    public int getmaxid() throws Exception{
        Company company = null;
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select MAX(company_id) from company"
        );
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //companyに結果を保存して返却
        while(rs.next()){
            company = new Company();
            company.setCompany_id(rs.getInt("MAX(company_id)"));
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return company.getCompany_id();
    }

    //名前検索をしてそのidを取得
    public List<Company> getpass(String name) throws Exception{
        //配列の作成
        List<Company> list=new ArrayList<Company>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from company where company_name=?"
        );
        st.setString(1, name);
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //company_listに結果を保存して返却
        while(rs.next()){
            Company company = new Company();
            company.setCompany_pass(rs.getString("company_pass"));
            //リストに追加
            list.add(company);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
    }

    //テーブル全取得
    public List<Company> allselect() throws Exception{
        //配列の作成
        List<Company> list=new ArrayList<Company>();
        //DBに接続
        Connection con = getConnection();
        //SQL文の作成
        PreparedStatement st=con.prepareStatement(
        "select * from company "
        );
        
    
        //SQL文の実行結果をrsに保存
        ResultSet rs=st.executeQuery();
    
        //company_listに結果を保存して返却
        while(rs.next()){
            Company company = new Company();
            company.setCompany_id(rs.getInt("company_id"));
            company.setCompany_name(rs.getString("company_name"));
            company.setCompany_addres(rs.getString("company_addres"));
            company.setCompany_mailaddres(rs.getString("company_mailaddres"));
            //リストに追加
            list.add(company);
        }
        //DBの接続切断
        st.close();
        con.close();
    
        //結果の返却
        return list;
    }

    //内容の変更
    public int update(String company_name, String company_addres, String company_mailaddres, int company_id){
        try {
            //DBに接続
            Connection con=getConnection();
            //SQL文の作成
            PreparedStatement st=con.prepareStatement(
            "UPDATE company SET company_name=?, company_addres=?, company_mailaddres=? WHERE company_id=?"
            );
            st.setString(1, company_name);
            st.setString(2, company_addres);
            st.setString(3, company_mailaddres);
            st.setInt(4, company_id);
            //変更された列数を取得
            int line=st.executeUpdate();
    
            //DBの接続切断
            st.close();
            con.close();
    
            //変更された列数を返却
            return line;
        } catch (Exception e) {
            return 0;
        }
    }
}

