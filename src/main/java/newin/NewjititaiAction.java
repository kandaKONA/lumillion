package newin;

import bean.*;
import dao.*;
import hash.Hashtest;
import tool.Action;
import javax.servlet.http.*;

public class NewjititaiAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        //HTMLのname="　　"を参照してそこに入力された情報を変数に保存
        String jititai_name = request.getParameter("jititai_name").replaceFirst("^[\\h]+", "");  //最初の空白を削除
        String jititai_addres = request.getParameter("jititai_addres").replaceFirst("^[\\h]+", "");
        String jititai_mailaddres = request.getParameter("jititai_mailaddres").replaceFirst("^[\\h]+", "");
        String jititai_pass = request.getParameter("jititai_pass").replaceFirst("^[\\h]+", "");

        //空白のみで登録されているかどうか
        if (jititai_name.equals("") || jititai_addres.equals("") || jititai_mailaddres.equals("")
        || jititai_pass.equals("")) {
            session.setAttribute("error", "new");
            response.sendRedirect("newjititai.jsp");
            return null;
        }

        //入力された情報をDBに追加
        Jititai jititai_in = new Jititai();
        JititaiDAO dao = new JititaiDAO();

        //ハッシュ化
        Hashtest hash = new Hashtest();
        String pass = hash.pass(jititai_pass);
        jititai_in.setJititai_name(jititai_name);
        jititai_in.setJititai_addres(jititai_addres);
        jititai_in.setJititai_mailaddres(jititai_mailaddres);
        jititai_in.setJititai_pass(pass);
        
        int insert = dao.insert(jititai_in);

        //追加された数が0かどうか
        if(insert == 0){
            //登録失敗ページに遷移
            return "newjititai.jsp";
        }else{
            //jititaiにユーザー情報を保存
            Jititai jititai = dao.select(jititai_name, pass);
            //jititaiに情報があるか
            if(jititai != null){
                //jititaiの情報をセッションに保存
                session.setAttribute("jititai", jititai);
                //正常なページ遷移
                response.sendRedirect("../jititai/jititai.jsp");
            }
        }

        //登録失敗ページに遷移
        return "";
    }
}
