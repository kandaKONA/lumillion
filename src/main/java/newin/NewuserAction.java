package newin;

import bean.*;
import dao.*;
import hash.Hashtest;
import login.UserloginAction;
import tool.Action;

import java.util.List;

import javax.servlet.http.*;

public class NewuserAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        //HTMLのname="　　"を参照してそこに入力された情報を変数に保存
        String user_name = request.getParameter("user_name").replaceFirst("^[\\h]+", "");  //最初の空白を削除
        String user_pass = request.getParameter("user_pass").replaceFirst("^[\\h]+", "");
        String user_tel = request.getParameter("user_tel").replaceFirst("^[\\h]+", "");

        //空白のみで登録されているかどうか
        if (user_name.equals("") || user_pass.equals("") || user_tel.equals("")) {
            session.setAttribute("error", "new");
            response.sendRedirect("newuser.jsp");
            return null;
        }
        UserDAO dao = new UserDAO();

        //電話番号が既に登録されているか
        List<User> usertel = dao.gettel();
        boolean tel_flag = false;
        for (int i = 0; i < usertel.size(); i++) {
            Hashtest hash = new Hashtest();
            //登録されていたらフラグをtrueに
            if (hash.mach(user_tel, usertel.get(i).getUser_tel())) {
                tel_flag = true;
                break;
            }
        }
        //フラグがtrueならエラー
        if (tel_flag) {
            session.setAttribute("error", "tel");
            response.sendRedirect("newuser.jsp");
            return null;
        }

        //入力された情報をDBに追加
        User user_in = new User();

        //ハッシュ化
        Hashtest hash = new Hashtest();
        String pass = hash.pass(user_pass);
        String hash_tel = hash.pass(user_tel);
        user_in.setUser_name(user_name);
        user_in.setUser_pass(pass);
        user_in.setUser_tel(hash_tel);
        int insert = dao.insert(user_in);

        //追加された数が0かどうか
        if(insert == 0){
            //登録失敗ページに遷移
            session.setAttribute("error", "new");
            response.sendRedirect("newuser.jsp");
            return "";
        }else{
            //userにユーザー情報を保存
            User user = dao.select(user_name, pass);
            //userに情報があるか
            if(user != null){
                //userの情報をセッションに保存
                session.setAttribute("user", user);
                //表示するデータの検索
                //イベント
                UserloginAction login = new UserloginAction();
                List<Event> allevent = login.allevent();
                if(allevent != null){
                    //全てのeventの情報をセッションに保存
                    session.setAttribute("allevent", allevent);
                }

                //参加リスト
                    List<Event> alljoin = login.alljoin(user);
                    session.setAttribute("alljoin", alljoin);

                    //メッセージ一覧
                    List<Event> allmess = login.allmess(user);
                    session.setAttribute("allmess", allmess);

                //正常なページ遷移
                response.sendRedirect("../user/user.jsp");
            }
        }

        //登録失敗ページに遷移
        return "";
    }
}
