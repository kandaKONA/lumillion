package newin;

import bean.*;
import dao.*;
import hash.*;
import tool.Action;
import javax.servlet.http.*;

public class NewcompanyAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        //HTMLのname="　　"を参照してそこに入力された情報を変数に保存
        String company_name = request.getParameter("company_name").replaceFirst("^[\\h]+", "");  //最初の空白を削除
        String company_addres = request.getParameter("company_addres").replaceFirst("^[\\h]+", "");
        String company_mailaddres = request.getParameter("company_mailaddres").replaceFirst("^[\\h]+", "");
        String company_pass = request.getParameter("company_pass").replaceFirst("^[\\h]+", "");

        //空白のみで登録されているかどうか
        if (company_name.equals("") || company_addres.equals("") || company_mailaddres.equals("")
        || company_pass.equals("")) {
            session.setAttribute("error", "new");
            response.sendRedirect("newcompany.jsp");
            return null;
        }

        //入力された情報をDBに追加
        CompanyDAO dao = new CompanyDAO();
        Company company_in = new Company();

        //ハッシュ化
        int id = dao.getmaxid() + 1;
        Hashtest hash = new Hashtest();
        String pass = hash.pass(company_pass);
        company_in.setCompany_id(id);
        company_in.setCompany_name(company_name);
        company_in.setCompany_addres(company_addres);
        company_in.setCompany_mailaddres(company_mailaddres);
        company_in.setCompany_pass(pass);
        int insert = dao.insert(company_in);

        //追加された数が0かどうか
        if(insert == 0){
            //登録失敗ページに遷移
            return "";
        }else{
            //companyにユーザー情報を保存
            Company company = dao.select(company_name, pass);
            //companyに情報があるか
            if(company != null){
                //companyの情報をセッションに保存
                session.setAttribute("company", company);
                //正常なページ遷移
                response.sendRedirect("../company/company.jsp");
            }
        }

        //登録失敗ページに遷移
        return "";
    }
}
