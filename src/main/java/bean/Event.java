package bean;

public class Event {
    //カラム名と型を宣言
    private int event_id;
    private int jititai4_id;
	private String event_title;
    private String event_day;
    private String event_place;
    private String event_content;
    private String event_reward;
    private int event_number;
    private String event_others;

    public int getevent_id(){
        return event_id;
    }
    public int getjititai4_id(){
        return jititai4_id;
    }
    public String getevent_title(){
        return event_title;
    }
    public String getevent_day(){
        return event_day;
    }
    public String getevent_place(){
        return event_place;
    }
    public String getevent_content(){
        return event_content;
    }
    public String getevent_reward(){
        return event_reward;
    }
    public int getevent_number(){
        return event_number;
    }
    public String getevent_others(){
        return event_others;
    }

    public void setevent_id(int event_id){
        this.event_id=event_id;
    }
    public void setjititai4_id(int jititai4_id){
        this.jititai4_id=jititai4_id;
    }
    public void setevent_title(String event_title){
        this.event_title=event_title;
    }
    public void setevent_day(String event_day){
        this.event_day=event_day;
    }
    public void setevent_place(String event_place){
        this.event_place=event_place;
    }
    public void setevent_content(String event_content){
        this.event_content=event_content;
    }
    public void setevent_reward(String event_reward){
        this.event_reward=event_reward;
    }
    public void setevent_number(int event_number){
        this.event_number=event_number;
    }
    public void setevent_others(String event_others){
        this.event_others=event_others;
    }
}
