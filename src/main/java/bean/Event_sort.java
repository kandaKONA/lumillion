package bean;

import java.util.Comparator;

public class Event_sort implements Comparator<Event>{

    public int compare(Event o1, Event o2) {
        String day1 = o1.getevent_day();
        String day2 = o2.getevent_day();
        if (day1.compareTo(day2) > 0) {
            return 1;

        } else if (day1 == day2) {
            return 0;

        } else {
            return -1;

        }
    }
    
}
