package bean;

import java.sql.Timestamp;

public class Message implements java.io.Serializable{
    private int mess_id;
    private int event2_id;
	private int company2_id;
	private int jititai2_id;
    private Timestamp mess_day;
    private String mess_text;
    private int flag;

    public int getMess_id(){
        return mess_id;
    }
    public int getEvent2_id(){
        return event2_id;
    }
    public int getCompany2_id(){
        return company2_id;
    }
    public int getJititai2_id(){
        return jititai2_id;
    }
    public Timestamp getMess_day(){
        return mess_day;
    }
    public String getMess_text(){
        return mess_text;
    }
    public int getFlag(){
        return flag;
    }

    public void setMess_id(int mess_id){
        this.mess_id=mess_id;
    }
    public void setEvent2_id(int event2_id){
        this.event2_id=event2_id;
    }
    public void setCompany2_id(int company2_id){
        this.company2_id=company2_id;
    }
    public void setJititai2_id(int jititai2_id){
        this.jititai2_id=jititai2_id;
    }
    public void setMess_day(Timestamp mess_day){
        this.mess_day=mess_day;
    }
    public void setMess_text(String mess_text){
        this.mess_text=mess_text;
    }
    public void setFlag(int flag){
        this.flag=flag;
    }
}
