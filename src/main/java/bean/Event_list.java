package bean;

import java.sql.Timestamp;

public class Event_list implements java.io.Serializable{
    private int user_id;
	private int event_id;
	private Timestamp creat_time;
    private int join_flag;
    private String read_check;

    public int getUser_id(){
        return user_id;
    }
    public int getEvent_id(){
        return event_id;
    }
    public Timestamp getCreat_time(){
        return creat_time;
    }
    public int getJoin_flag(){
        return join_flag;
    }
    public String getRead_check(){
        return read_check;
    }

    public void setUser_id(int user_id){
        this.user_id=user_id;
    }
    public void setEvent_id(int event_id){
        this.event_id=event_id;
    }
    public void setCreat_time(Timestamp creat_time){
        this.creat_time=creat_time;
    }
    public void setJoin_flag(int join_flag){
        this.join_flag=join_flag;
    }
    public void setRead_check(String read_check){
        this.read_check=read_check;
    }
}
