package bean;

public class Company implements java.io.Serializable{
    private int company_id;
	private String company_name;
    private String company_addres;
    private String company_mailaddres;
	private String company_pass;

    public int getCompany_id(){
        return company_id;
    }
    public String getCompany_name(){
        return company_name;
    }
    public String getCompany_addres(){
        return company_addres;
    }
    public String getCompany_mailaddres(){
        return company_mailaddres;
    }
    public String getCompany_pass(){
        return company_pass;
    }

    public void setCompany_id(int company_id){
        this.company_id=company_id;
    }
    public void setCompany_name(String company_name){
        this.company_name=company_name;
    }
    public void setCompany_addres(String company_addres){
        this.company_addres=company_addres;
    }
    public void setCompany_mailaddres(String company_mailaddres){
        this.company_mailaddres=company_mailaddres;
    }
    public void setCompany_pass(String company_pass){
        this.company_pass=company_pass;
    }
}