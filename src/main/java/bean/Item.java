package bean;

public class Item implements java.io.Serializable{
    private int item_id;
    private int company3_id;
	private String item_name;
	private String item_text;
    private byte[] item_image;

    public int getItem_id(){
        return item_id;
    }
    public int getCompany3_id(){
        return company3_id;
    }
    public String getItem_name(){
        return item_name;
    }
    public String getItem_text(){
        return item_text;
    }
    public byte[] getItem_image(){
        return item_image;
    }

    public void setItem_id(int item_id){
        this.item_id=item_id;
    }
    public void setCompany3_id(int company3_id){
        this.company3_id=company3_id;
    }
    public void setItem_name(String item_name){
        this.item_name=item_name;
    }
    public void setItem_text(String item_text){
        this.item_text=item_text;
    }
    public void setItem_image(byte[] item_image){
        this.item_image=item_image;
    }
}
