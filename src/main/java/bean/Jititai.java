package bean;

public class Jititai implements java.io.Serializable{
    private int jititai_id;
	private String jititai_name;
    private String jititai_addres;
    private String jititai_mailaddres;
	private String jititai_pass;

    public int getJititai_id(){
        return jititai_id;
    }
    public String getJititai_name(){
        return jititai_name;
    }
    public String getJititai_addres(){
        return jititai_addres;
    }
    public String getJititai_mailaddres(){
        return jititai_mailaddres;
    }
    public String getJititai_pass(){
        return jititai_pass;
    }

    public void setJititai_id(int jititai_id){
        this.jititai_id=jititai_id;
    }
    public void setJititai_name(String jititai_name){
        this.jititai_name=jititai_name;
    }
    public void setJititai_addres(String jititai_addres){
        this.jititai_addres=jititai_addres;
    }
    public void setJititai_mailaddres(String jititai_mailaddres){
        this.jititai_mailaddres=jititai_mailaddres;
    }
    public void setJititai_pass(String jititai_pass){
        this.jititai_pass=jititai_pass;
    }
}