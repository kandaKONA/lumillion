package bean;

public class Event2 {
    //カラム名と型を宣言
    private int event2_id;
    private int jititai5_id;
	private String event2_title;
    private String event2_day;
    private String event2_text;
    private String event2_place;

    public int getevent2_id(){
        return event2_id;
    }
    public int getjititai5_id(){
        return jititai5_id;
    }
    public String getevent2_title(){
        return event2_title;
    }
    public String getevent2_day(){
        return event2_day;
    }
    public String getevent2_text(){
        return event2_text;
    }
    public String getevent2_place(){
        return event2_place;
    }

    public void setevent2_id(int event2_id){
        this.event2_id=event2_id;
    }
    public void setjititai5_id(int jititai5_id){
        this.jititai5_id=jititai5_id;
    }
    public void setevent2_title(String event2_title){
        this.event2_title=event2_title;
    }
    public void setevent2_day(String event2_day){
        this.event2_day=event2_day;
    }
    public void setevent2_text(String event2_text){
        this.event2_text=event2_text;
    }
    public void setevent2_place(String event2_place){
        this.event2_place=event2_place;
    }
}