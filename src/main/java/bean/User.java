package bean;

public class User implements java.io.Serializable{
    private int user_id;
	private String user_name;
	private String user_pass;
    private String user_tel;
    private int user_ngcount;

    public int getUser_id(){
        return user_id;
    }
    public String getUser_name(){
        return user_name;
    }
    public String getUser_pass(){
        return user_pass;
    }
    public String getUser_tel(){
        return user_tel;
    }
    public int getUser_ngcount(){
        return user_ngcount;
    }

    public void setUser_id(int user_id){
        this.user_id=user_id;
    }
    public void setUser_name(String user_name){
        this.user_name=user_name;
    }
    public void setUser_pass(String user_pass){
        this.user_pass=user_pass;
    }
    public void setUser_tel(String user_tel){
        this.user_tel=user_tel;
    }
    public void setUser_ngcount(int user_ngcount){
        this.user_ngcount=user_ngcount;
    }
}
