package bean;

import java.sql.Timestamp;

public class User_message implements java.io.Serializable{
    private int umess_id;
    private int event3_id;
	private int userfk_id;
	private int jititai3_id;
    private Timestamp umess_day;
    private String umess_text;

    public int getUmess_id(){
        return umess_id;
    }
    public int getEvent3_id(){
        return event3_id;
    }
    public int getUserfk_id(){
        return userfk_id;
    }
    public int getJititai3_id(){
        return jititai3_id;
    }
    public Timestamp getUmess_day(){
        return umess_day;
    }
    public String getUmess_text(){
        return umess_text;
    }

    public void setUmess_id(int umess_id){
        this.umess_id=umess_id;
    }
    public void setEvent3_id(int event3_id){
        this.event3_id=event3_id;
    }
    public void setUserfk_id(int userfk_id){
        this.userfk_id=userfk_id;
    }
    public void setJititai3_id(int jititai3_id){
        this.jititai3_id=jititai3_id;
    }
    public void setUmess_day(Timestamp umess_day){
        this.umess_day=umess_day;
    }
    public void setUmess_text(String umess_text){
        this.umess_text=umess_text;
    }
}

