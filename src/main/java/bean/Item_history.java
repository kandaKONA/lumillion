package bean;

import java.sql.Timestamp;

public class Item_history implements java.io.Serializable{
    private int history_id;
	private int item_id;
	private int jititai_id;
    private int company_id;
    private int event2_id;
    private Timestamp creat_time;
    private String read_check;

    public int getHistory_id(){
        return history_id;
    }
    public int getItem_id(){
        return item_id;
    }
    public int getJititai_id(){
        return jititai_id;
    }
    public int getCompany_id(){
        return company_id;
    }
    public int getEvent2_id(){
        return event2_id;
    }
    public Timestamp getCreat_time(){
        return creat_time;
    }
    public String getRead_check(){
        return read_check;
    }


    public void setHistory_id(int history_id){
        this.history_id=history_id;
    }
    public void setItem_id(int item_id){
        this.item_id=item_id;
    }
    public void setJititai_id(int jititai_id){
        this.jititai_id=jititai_id;
    }
    public void setCompany_id(int company_id){
        this.company_id=company_id;
    }
    public void setEvent2_id(int event2_id){
        this.event2_id=event2_id;
    }
    public void setCreat_time(Timestamp creat_time){
        this.creat_time=creat_time;
    }
    public void setRead_check(String read_check){
        this.read_check = read_check;
    }
}
