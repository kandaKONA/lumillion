package company;

import bean.*;
import dao.*;
import tool.Action;

import java.util.List;

import javax.servlet.http.*;

public class EventitemAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの開始
        HttpSession session=request.getSession();
        //選択された景品のidを取得
        String id = request.getParameter("event2_id");

        //文字列から数値に変換
        int event2_id = Integer.parseInt(id);
        Event2DAO evedao = new Event2DAO();
        Event2 event2 = evedao.select(event2_id);

        if (event2 != null) {
            session.setAttribute("selectevent2", event2);
        }

        //企業IDからその企業の景品一覧を取得
        Company company = (Company) session.getAttribute("company");
        ItemDAO itedao = new ItemDAO();
        List<Item> allitem = itedao.allselect(company.getCompany_id());

        if (allitem != null) {
            session.setAttribute("formitem", allitem);
        }   

        //正常なページ遷移
        response.sendRedirect("eventitem.jsp");

        return "";
    }
}