package company;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;

public class ItemselectAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの開始
        HttpSession session=request.getSession();

        //選択された景品のidを取得
        String id = request.getParameter("item_id");

        //文字列から数値に変換
        int item_id = Integer.parseInt(id);

        //DBに接続して景品情報を取得
        ItemDAO dao = new ItemDAO();
        Item item = dao.select(item_id);

        if(item != null){
            //1つのitemの情報をセッションに保存
            session.setAttribute("item", item);
            
            //正常なページ遷移
            response.sendRedirect("itemmanegment.jsp");
        }

        return "";
    }
}