package company;

import dao.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import javax.imageio.ImageIO;

@WebServlet(urlPatterns = {"/company/img"})
public class ItemimageAction extends HttpServlet {
  public void doGet (HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String id = request.getParameter("item_id");
        
        //idを文字から数字列に変更
        int item_id = Integer.parseInt(id);

        //DBに接続して画像を取得
        ItemDAO dao = new ItemDAO();
        BufferedImage img;
        try {
          img = dao.selectimage(item_id);
          // 画像をクライアントに返却する
		    response.setContentType("image/jpeg");
		    OutputStream os = response.getOutputStream();
		    ImageIO.write(img, "png", os);
		    os.flush();
        } catch (Exception e) {
          e.printStackTrace();
        }
			
		
    }
    
}
