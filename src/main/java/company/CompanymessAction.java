package company;

import bean.*;
import dao.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;

public class CompanymessAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの開始
        HttpSession session=request.getSession();

        String event2 = request.getParameter("event2_id");
        String jititai = request.getParameter("jititai_id");
        Company com = (Company) session.getAttribute("company");

        //文字列から数値に変換
        int event2_id = Integer.parseInt(event2);
        int jititai_id = Integer.parseInt(jititai);

        Message mess = new Message();
        mess.setCompany2_id(com.getCompany_id());
        mess.setEvent2_id(event2_id);
        mess.setJititai2_id(jititai_id);
        //メッセージ取得
        MessageDAO messdao = new MessageDAO();
        List<Message> allmess = messdao.limitslecttalk(mess);

        //既読かどうか
        Item_history read = new Item_history();
        Item_historyDAO itemdao = new Item_historyDAO();
        read.setCompany_id(com.getCompany_id());
        read.setEvent2_id(event2_id);
        read.setJititai_id(jititai_id);
        read=itemdao.select_read(read);
        if (read.getRead_check().equals("企業未読")) {
            read.setRead_check("既読");
            itemdao.change_read(read);
        }

        if (allmess != null) {
            session.setAttribute("comallmess", allmess);
            //正常なページ遷移
            response.sendRedirect("companymess.jsp");
        }

        return "";
    }
    
}
