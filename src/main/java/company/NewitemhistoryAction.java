package company;

import bean.*;
import dao.*;
import tool.Action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.*;

public class NewitemhistoryAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの開始
        HttpSession session=request.getSession();
        
        Company company = (Company) session.getAttribute("company");
        Event2 event2 = (Event2) session.getAttribute("selectevent2");
        Item item = (Item) session.getAttribute("reitem");

        //景品が選択されていない場合
        if (item == null) {
            session.setAttribute("error", "item");
            response.sendRedirect("eventitem.jsp");
            return null;
        }

        Event2DAO evedao = new Event2DAO();
        event2 = evedao.select(event2.getevent2_id());

        
        Item_history item_his = new Item_history();
        item_his.setCompany_id(company.getCompany_id());
        item_his.setEvent2_id(event2.getevent2_id());
        item_his.setItem_id(item.getItem_id());
        item_his.setJititai_id(event2.getjititai5_id());
        item_his.setRead_check("自治体未読");

        Item_historyDAO historydao = new Item_historyDAO();
        int line = historydao.insert(item_his);

        if (line != 0) {
            Message mess = new Message();
            mess.setCompany2_id(item_his.getCompany_id());
            mess.setEvent2_id(item_his.getEvent2_id());
            mess.setJititai2_id(item_his.getJititai_id());
            mess.setFlag(0);//企業が０
            String text = "景品名\n" + item.getItem_name() + "\n景品説明\n" + item.getItem_text();
            mess.setMess_text(text); 

            MessageDAO messdao = new MessageDAO();
            int messline = messdao.insert(mess);

            if (messline != 0) {
                //イベントの情報を保存
                Event2DAO eventdao = new Event2DAO();
                List<Event2> allevent2 = eventdao.slectall();
                if (allevent2 != null) {
                    session.setAttribute("comallevent2", allevent2);
                }
                //メッセージの情報を保存
                Item_history item2 = new Item_history();
                item2.setCompany_id(company.getCompany_id());
                //メッセージをやり取りしているイベント一覧
                Item_historyDAO itemdao = new Item_historyDAO();
                List<Item_history> his = itemdao.selectallcom(item2);
                if (his != null) {
                    //自治体名、イベント名、景品名を取得する
                    Jititai jiti = new Jititai();
                    Event2 eve = new Event2();
                    Item ite = new Item();
                    List<Jititai> jitilist = new ArrayList<Jititai>();
                    List<Event2> evelist = new ArrayList<Event2>();
                    List<Item> itelist = new ArrayList<Item>();
                    JititaiDAO jitidao = new JititaiDAO();
                    ItemDAO itedao = new ItemDAO();
                    int count = 0;

                    for (int i = 0; i < his.size(); i++) {
                        jiti = jitidao.select(his.get(i).getJititai_id());
                    eve = evedao.select(his.get(i).getEvent2_id());
                        ite = itedao.select(his.get(i).getItem_id());

                        if (jiti != null && eve != null && ite != null) {
                            jitilist.add(jiti);
                            evelist.add(eve);
                            itelist.add(ite);
                            count++;
                            if (count == 5) {
                                break;
                            }
                        }

                    }
                    //全てのeventの情報をセッションに保存
                    session.setAttribute("jitilist5c", jitilist);
                    session.setAttribute("evelist5c", evelist);
                    session.setAttribute("itelist5c", itelist);
                    session.setAttribute("size5c", jitilist.size());

                //正常なページ遷移
                response.sendRedirect("company.jsp");
                }
            }
            
        }

        return "";
    }
    
}
