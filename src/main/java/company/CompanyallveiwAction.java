package company;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import java.util.ArrayList;
import java.util.List;

public class CompanyallveiwAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();
        
        String gen = request.getParameter("comgen");

        //送られてきたgenでどの情報を引き出すか決める
        //イベント
        if (gen.equals("event2")) {
            Event2DAO event2dao = new Event2DAO();
            List<Event2> viewevent2 = event2dao.limitslect();

            if (viewevent2 != null) {

                session.setAttribute("viewevent2", viewevent2);
                session.setAttribute("comgen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }

        } else if (gen.equals("mess") || gen.equals("history")) {
            //企業idを取得後beanにセット
            Company company = (Company) session.getAttribute("company");
            Item_history item = new Item_history();
            item.setCompany_id(company.getCompany_id());
            //メッセージをやり取りしているイベント一覧
            Item_historyDAO itemdao = new Item_historyDAO();
            List<Item_history> his = itemdao.selectallcom(item);

            if (his != null) {
                //自治体名、イベント名、景品名を取得する
                Jititai jiti = new Jititai();
                Event2 eve = new Event2();
                Item ite = new Item();
                List<Jititai> jitilist = new ArrayList<Jititai>();
                List<Event2> evelist = new ArrayList<Event2>();
                List<Item> itelist = new ArrayList<Item>();
                JititaiDAO jitidao = new JititaiDAO();
                Event2DAO evedao = new Event2DAO();
                ItemDAO itedao = new ItemDAO();

                //既読チェック
                List<String> comread = new ArrayList<String>();

                for (int i = 0; i < his.size(); i++) {
                    jiti = jitidao.select(his.get(i).getJititai_id());
                    eve = evedao.select(his.get(i).getEvent2_id());
                    ite = itedao.select(his.get(i).getItem_id());
                    comread.add(his.get(i).getRead_check());

                    if (jiti != null && eve != null && ite != null) {
                        jitilist.add(jiti);
                        evelist.add(eve);
                        itelist.add(ite);
                    }

                }
                //全てのeventの情報をセッションに保存
                session.setAttribute("jitilist", jitilist);
                session.setAttribute("evelist", evelist);
                session.setAttribute("itelist", itelist);
                session.setAttribute("size", jitilist.size());
                session.setAttribute("comgen", gen);
                session.setAttribute("allcomread", comread);
                System.out.println(comread.get(0));
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            
            }

        }else if(gen.equals("item")){
            Company company = (Company) session.getAttribute("company");
            ItemDAO itedao = new ItemDAO();
            List<Item> allitem = itedao.allselect(company.getCompany_id());

            if (allitem != null) {
                session.setAttribute("allitem", allitem);
                session.setAttribute("comgen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }

        }
    
        return "";    
    }
}
