package company;

import bean.*;
import dao.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;
import java.util.ArrayList;

public class SearchAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String search = request.getParameter("search").replaceFirst("^[\\h]+", "");  //最初の空白を削除
        String place = request.getParameter("pref").replaceFirst("^[\\h]+", "");
        String sort = request.getParameter("sort");

        List<Event2> allevent = new ArrayList<Event2>();
        Event2DAO dao = new Event2DAO();
        if (search.equals("") && place.equals("")) {
            //どちらも未入力なら最新五件
            allevent = dao.slectall(sort);
            
        } else if (search.equals("")) {
            //県のみ入力なら場所検索
            allevent = dao.place_searchlimit(place, sort);
            
        } else if(place.equals("")){
            //キーワードのみ入力ならキーワード検索
            allevent = dao.keyword_searchlimit(search, sort);
            
        } else{
            //どちらも入力済みなら場所且つキーワード検索
            allevent = dao.place_keyword_searchlimit(place, search, sort);
            
        }

        System.out.println(allevent);
        if(allevent.size() != 0){
            //全てのeventの情報をセッションに保存
            session.setAttribute("comallevent2", allevent);
            //正常なページ遷移
            response.sendRedirect("company.jsp");
        }else{
            //全てのeventの情報をセッションに保存
            session.setAttribute("comallevent2", allevent);
            //正常なページ遷移
            response.sendRedirect("company.jsp");
        }
        
        return "";
    }
}