package company;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.List;

public class ChangeinfoAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws ServletException,Exception {
        //セッションの確認
        HttpSession session=request.getSession();

        Company company = (Company) session.getAttribute("company");
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String company_name = request.getParameter("company_name");
        String company_addres = request.getParameter("company_addres");
        String company_mailaddres = request.getParameter("company_mailaddres");
        company_name = company_name.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        company_addres = company_addres.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        company_mailaddres = company_mailaddres.replaceFirst("^[\\h]+", "");  //最初の空白を削除

        //空白のみで登録されているかどうか
        if (company_name.equals("")) {
            session.setAttribute("error", "change");
            response.sendRedirect("companymanegment.jsp");
            return null;
        }

        //sqlのUPDATE実行
        CompanyDAO dao = new CompanyDAO();
        int change = dao.update(company_name, company_addres, company_mailaddres, company.getCompany_id());

        if (change == 0) {
            //変更失敗ページに遷移
            return "changecompany.jsp";
        } else {
            List<Company> allcompany = dao.allselect();
            //全てのcompanyの情報をセッションに保存
            session.setAttribute("allcompany", allcompany);
            //正常なページ遷移
            response.sendRedirect("company.jsp");
        }
        return "";
    }
}
