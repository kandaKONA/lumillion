package company;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import javax.servlet.*;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;


@MultipartConfig
public class NewitemAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws ServletException,Exception {
        //セッションの確認
        HttpSession session=request.getSession();

        Company com = (Company) session.getAttribute("company");
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String item_name = request.getParameter("item_name");
        String item_text = request.getParameter("item_text");
        item_name = item_name.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        item_text = item_text.replaceFirst("^[\\h]+", "");

        //空白のみで登録されているかどうか
        if (item_name.equals("") && item_text.equals("")) {
            session.setAttribute("error", "new");
            response.sendRedirect("newitem.jsp");
            return null;
        }
        //画像データの参照
        InputStream item_image= null;
        Part filePart = request.getPart("image");
        if (filePart != null) {
            item_image = filePart.getInputStream();
        }

        //画像を保存するbyte[]を作成
        byte[] item_img;

        //画像データをbyte列に変換してitem_imgに保存
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int read;
            byte[] data = new byte[1024];
            while ((read = item_image.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, read);
            }
            
            item_img = buffer.toByteArray();

        //beanのitemに保存
        Item item_in = new Item();
        item_in.setItem_name(item_name);
        item_in.setCompany3_id(com.getCompany_id());
        item_in.setItem_text(item_text);
        item_in.setItem_image(item_img);

        ItemDAO dao = new ItemDAO();
        int insert = dao.insert(item_in);

        if (insert == 0) {
            //登録失敗ページに遷移
            return "company.jsp";
        } else {
            List<Item> allitem = dao.allselect();
            //全てのitemの情報をセッションに保存
            session.setAttribute("allitem", allitem);
            //正常なページ遷移
            response.sendRedirect("company.jsp");
            
        }

        return "";
    }
    
}
