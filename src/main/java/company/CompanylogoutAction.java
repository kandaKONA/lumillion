package company;

import tool.Action;
import javax.servlet.http.*;

public class CompanylogoutAction extends Action {
	public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
		//セッションの確認
        HttpSession session=request.getSession();
		//既にログインしているか
        if (session.getAttribute("company")!=null) {
			//セッションの破棄
			session.removeAttribute("company");
			session.invalidate();
			//正常なページ遷移
			response.sendRedirect("../login/companylogin.jsp");
		}
		//エラーの時のページ遷移
		
        return "../login/companylogin.jsp";
    }
    
}
