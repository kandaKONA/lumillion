package company;

import bean.*;
import dao.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;
import java.util.ArrayList;

public class AllsearchAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String search = request.getParameter("allsearch").replaceFirst("^[\\h]+", "");  //最初の空白を削除
        String place = request.getParameter("allpref").replaceFirst("^[\\h]+", "");
        String sort = request.getParameter("allsort");

        List<Event2> viewevent = new ArrayList<Event2>();
        Event2DAO dao = new Event2DAO();
        if (search.equals("") && place.equals("")) {
            //どちらも未入力なら全て
            viewevent = dao.limitslect(sort);
            
        } else if (search.equals("")) {
            //県のみ入力なら場所検索
            viewevent = dao.place_search(place, sort);
            
        } else if(place.equals("")){
            //キーワードのみ入力ならキーワード検索
            viewevent = dao.keyword_search(search, sort);
            
        } else{
            //どちらも入力済みなら場所且つキーワード検索
            viewevent = dao.place_keyword_search(place, search, sort);
            
        }

        System.out.println(viewevent);
        if(viewevent.size() != 0){
            //全てのeventの情報をセッションに保存
            session.setAttribute("viewevent2", viewevent);
            //正常なページ遷移
            response.sendRedirect("allview.jsp");
        }else{
            //全てのeventの情報をセッションに保存
            session.setAttribute("viewevent2", viewevent);
            //正常なページ遷移
            response.sendRedirect("allview.jsp");
        }
        
        return "";
    }
}