package login;

import bean.*;
import dao.*;
import hash.Hashtest;
import tool.Action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.*;

public class UserloginAction extends Action{
    //イベント一覧
    public List<Event> allevent() throws Exception{
        EventDAO eventdao = new EventDAO();
        List<Event> allevent = eventdao.slectall();

        return allevent;
    }

    //参加リスト
    public List<Event> alljoin(User user) throws Exception{
        Event_listDAO event_listdao = new Event_listDAO();
        //イベントリストテーブルからユーザーIDで検索
        List<Event_list> viewjoin = event_listdao.selectjoin(user.getUser_id());
        if (viewjoin != null) {
            EventDAO dao2 = new EventDAO();
            List<Event> alljoin = new ArrayList<Event>();
            Event event = new Event();
            int count = 0;
            for (int i = 0; i < viewjoin.size(); i++) {
                //イベントIDで検索
                event = dao2.selectday(viewjoin.get(i).getEvent_id());
                if (event != null) {
                    alljoin.add(event);
                    count++;
                    if(count == 5){
                        break;
                    }
                }
            }
            Collections.sort(alljoin, new Event_sort());
            return alljoin;
        }
        return null;
    }

    //メッセージ一覧
    public List<Event> allmess(User user) throws Exception{
        Event_listDAO event_listdao = new Event_listDAO();
        List<Event_list> viewjoin = event_listdao.selectjoin(user.getUser_id());
        EventDAO dao2 = new EventDAO();
        if (viewjoin != null) {
            int count = 0;
            Event event = new Event();
            List<Event> allmess = new ArrayList<Event>();
            for (int i = 0; i < viewjoin.size(); i++) {
                event = dao2.select(viewjoin.get(i).getEvent_id());
                if (event != null) {
                    allmess.add(event);
                    count++;
                    if(count == 5){
                        break;
                    }
                }
            }
            return allmess;
        }

        return null;
    }

    //メッセージ一覧既読未読
    public List<String> allmessread(User user) throws Exception{
        Event_listDAO event_listdao = new Event_listDAO();
        List<Event_list> viewjoin = event_listdao.selectjoin(user.getUser_id());
        EventDAO dao2 = new EventDAO();
        if (viewjoin != null) {
            int count = 0;
            Event event = new Event();
            List<String> allmess = new ArrayList<String>();
            for (int i = 0; i < viewjoin.size(); i++) {
                event = dao2.select(viewjoin.get(i).getEvent_id());
                if (event != null) {
                    allmess.add(viewjoin.get(i).getRead_check());
                    count++;
                    if(count == 5){
                        break;
                    }
                }
            }
            return allmess;
        }

        return null;
    }

    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        try {
            //セッションの取得
             HttpSession session=request.getSession();

            //HTMLのname="　　"を参照してそこに入力された情報を変数に保存
            String user_name = request.getParameter("tel");
            String user_pass = request.getParameter("password");

            UserDAO dao = new UserDAO();

            //ユーザーを検索
            List<User> pass = dao.getpass(user_name);
            User user = null;
            for (int i = 0; i < pass.size(); i++) {
                Hashtest hash = new Hashtest();

                if (hash.mach(user_pass, pass.get(i).getUser_pass())) {
                    user = dao.select(user_name, pass.get(i).getUser_pass());
                    break;
                }
            }

            //userに情報があるか
            if(user != null){
            //userの情報をセッションに保存
            session.setAttribute("user", user);

            //表示するデータの検索
            //イベント
            List<Event> allevent = allevent();
            
            if(allevent != null){
                //全てのeventの情報をセッションに保存
                session.setAttribute("allevent", allevent);
            }

            //参加リスト
                List<Event> alljoin = alljoin(user);
                //ソート
                
                session.setAttribute("alljoin", alljoin);

                //メッセージ一覧
                List<Event> allmess = allmess(user);
                System.out.println(allmess.size());
                session.setAttribute("allmess", allmess);

                List<String> read_check = allmessread(user);
                session.setAttribute("allmessread", read_check);

            //正常なページ遷移
            response.sendRedirect("../user/user.jsp");
        }else{
            session.setAttribute("error", "login");
            response.sendRedirect("userlogin.jsp");
        }
            return " ";
            
        } catch (Exception e) {
            System.out.println(e);
            return "userlogin.jsp";
        } 
        }
        
        
        
    
}
