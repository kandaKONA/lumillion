package login;

import tool.Action;
import javax.servlet.http.*;

public class LoginAction extends Action {
	public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        
        //セッションの開始
		HttpSession session=request.getSession();

        //HTMLのname="　　"を参照してそこに入力された情報を変数に保存
		String login=request.getParameter("login");
		String password=request.getParameter("password");

        //setAttribute("好きな名前", 登録したい情報)を入力する。
        //"好きな名前"をjsp側で使うと「登録したい情報」が表示される
        session.setAttribute("login", login);
        session.setAttribute("password", password);

        //正常なページ遷移
        response.sendRedirect("sessiontest.jsp");

        //エラーの時のページ遷移
        return "";
		
	}
}
