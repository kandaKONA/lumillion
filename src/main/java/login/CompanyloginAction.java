package login;

import bean.*;
import dao.*;
import tool.Action;
import hash.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.*;

public class CompanyloginAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        try {
            //セッションの取得
            HttpSession session=request.getSession();
            session.setMaxInactiveInterval(3600);

            //HTMLのname="　　"を参照してそこに入力された情報を変数に保存
            String company_name = request.getParameter("tel");
            String company_pass = request.getParameter("password");

            CompanyDAO dao = new CompanyDAO();
            Company company = null;
            //企業検索
            List<Company> pass = dao.getpass(company_name);
            for (int i = 0; i < pass.size(); i++) {
                Hashtest hash = new Hashtest();

                if (hash.mach(company_pass, pass.get(i).getCompany_pass())) {
                    company = dao.select(company_name, pass.get(i).getCompany_pass());
                    break;
                }
            }
            
            if(company != null){
                //companyの情報をセッションに保存
                session.setAttribute("company", company);

                //イベントの情報を保存
                Event2DAO eventdao = new Event2DAO();
                List<Event2> allevent2 = eventdao.slectall();
                if (allevent2 != null) {
                    session.setAttribute("comallevent2", allevent2);
                }
                //メッセージの情報を保存
                Item_history item = new Item_history();
                item.setCompany_id(company.getCompany_id());
                //メッセージをやり取りしているイベント一覧
                Item_historyDAO itemdao = new Item_historyDAO();
                List<Item_history> his = itemdao.selectallcom(item);
                if (his != null) {
                    //自治体名、イベント名、景品名を取得する
                    Jititai jiti = new Jititai();
                    Event2 eve = new Event2();
                    Item ite = new Item();
                    List<Jititai> jitilist = new ArrayList<Jititai>();
                    List<Event2> evelist = new ArrayList<Event2>();
                    List<Item> itelist = new ArrayList<Item>();
                    JititaiDAO jitidao = new JititaiDAO();
                    Event2DAO evedao = new Event2DAO();
                    ItemDAO itedao = new ItemDAO();
                    int count = 0;

                    //既読チェック
                    List<String> comread = new ArrayList<String>();

                    for (int i = 0; i < his.size(); i++) {
                        jiti = jitidao.select(his.get(i).getJititai_id());
                        eve = evedao.select(his.get(i).getEvent2_id());
                        ite = itedao.select(his.get(i).getItem_id());
                        comread.add(his.get(i).getRead_check());

                        if (jiti != null && eve != null && ite != null) {
                            jitilist.add(jiti);
                            evelist.add(eve);
                            itelist.add(ite);
                            count++;
                            if (count == 5) {
                                break;
                            }
                        }

                    }
                    //全てのeventの情報をセッションに保存
                    session.setAttribute("jitilist5c", jitilist);
                    session.setAttribute("evelist5c", evelist);
                    session.setAttribute("itelist5c", itelist);
                    session.setAttribute("size5c", jitilist.size());
                    session.setAttribute("comread", comread);

                //正常なページ遷移
                response.sendRedirect("../company/company.jsp");
            }
            
          }else{
            session.setAttribute("error", "login");
            response.sendRedirect("companylogin.jsp");
        }
          return"";
        } catch (Exception e) {
           return"";
        }
    }
}
