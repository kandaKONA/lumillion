package tool;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

@MultipartConfig
@WebServlet(urlPatterns={"*.action"})
public class FrontController extends HttpServlet {

	public void doPost(
		HttpServletRequest request, HttpServletResponse response
	) throws ServletException, IOException {
		//送られてきたものをoutに保存
		PrintWriter out=response.getWriter();
		try {

			String path=request.getServletPath().substring(1);
			//pathの名前の変更をnameに保存
			String name=path.replace(".a", "A").replace('/', '.');
			//nameに保存された名前でクラスを作成しそれをactionに保存
			Action action=(Action)Class.forName(name).
				getDeclaredConstructor().newInstance();
			//
			String url=action.execute(request, response);
			request.getRequestDispatcher(url).
				forward(request, response);
		} catch (Exception e) {
			e.printStackTrace(out);
		}
	}

	public void doGet(
		HttpServletRequest request, HttpServletResponse response
	) throws ServletException, IOException {
		doPost(request, response);
	}
}
