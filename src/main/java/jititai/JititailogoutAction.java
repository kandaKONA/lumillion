package jititai;

import tool.Action;
import javax.servlet.http.*;

public class JititailogoutAction extends Action {
	public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
		//セッションの確認
        HttpSession session=request.getSession();
		//既にログインしているか
        if (session.getAttribute("jititai")!=null) {
			//セッションの破棄
			session.removeAttribute("jititai");
			//session.invalidate();
			//正常なページ遷移
			response.sendRedirect("../login/jititailogin.jsp");
		}
		//エラーの時のページ遷移
        return "../login/jititailogin.jsp";
    }
    
}
