package jititai;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import javax.servlet.*;

import java.util.ArrayList;
import java.util.List;

public class ChangeEvent2Action extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws ServletException,Exception {
        //セッションの確認
        HttpSession session=request.getSession();

        Event2 event2 = (Event2) session.getAttribute("event2date");
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String event2_title = request.getParameter("event2_title");
        String event2_text = request.getParameter("event2_text");
        String event2_day = request.getParameter("event2_day");
        String mess_text = request.getParameter("textarea").replaceFirst("^[\\h]+", "");

        event2_title = event2_title.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event2_text = event2_text.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event2_day = event2_day.replaceFirst("^[\\h]+", "");  //最初の空白を削除

        //空白のみで登録されているかどうか
        if (event2_title.equals("") && event2_day.equals("")&& event2_text.equals("")
        && mess_text.equals("")){
            session.setAttribute("error", "change");
            response.sendRedirect("changeevent2.jsp");
            return null;
        }

        //sqlのUPDATE実行
        Event2DAO dao = new Event2DAO();
        System.out.println(event2.getevent2_id());
        int change = dao.update(event2_title, event2_day, event2_text, event2.getevent2_id());

        if (change == 0) {
            //変更失敗ページに遷移
            return "event2list.jsp";
        } else {

            //メッセージ登録
            List<Item_history> his = new ArrayList<Item_history>();
            Item_historyDAO hisdao = new Item_historyDAO();
            his = hisdao.selectallevent(event2.getevent2_id());
            Message mess = new Message();
            MessageDAO messdao = new MessageDAO();
            mess.setEvent2_id(event2.getevent2_id());
            mess.setJititai2_id(event2.getjititai5_id());
            mess.setFlag(1);
            mess.setMess_text(mess_text);
            for (int i = 0; i < his.size(); i++) {
                mess.setCompany2_id(his.get(i).getCompany_id());
                messdao.insert(mess);
            }
            //企業イベント
            Event2DAO event2dao = new Event2DAO();
            List<Event2> viewcevent = event2dao.slectall(event2.getjititai5_id());
            event2 = event2dao.select(event2.getevent2_id());
            if (viewcevent != null) {
                session.setAttribute("jitiallevent2", viewcevent);
                session.setAttribute("event2date", event2);
            }
            
            //正常なページ遷移
            response.sendRedirect("eventlist2.jsp");
        }
        return "";
    }
}
