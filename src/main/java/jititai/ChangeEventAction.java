package jititai;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import javax.servlet.*;

import java.util.ArrayList;
import java.util.List;

public class ChangeEventAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws ServletException,Exception {
        //セッションの確認
        HttpSession session=request.getSession();

        Event event = (Event) session.getAttribute("eventdate");
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String event_title = request.getParameter("event_title");
        String event_day = request.getParameter("event_day");
        String event_place = request.getParameter("event_place");
        String event_content = request.getParameter("event_content");
        String event_reward = request.getParameter("event_reward");
        String event_number = request.getParameter("event_number");
        String event_others = request.getParameter("event_others");
        String mess_text = request.getParameter("mess_text").replaceFirst("^[\\h]+", "");
        String mess_text2 = "" ;

        event_title = event_title.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event_day = event_day.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event_place = event_place.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event_content = event_content.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event_reward = event_reward.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event_number = event_number.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        event_others = event_others.replaceFirst("^[\\h]+", "");  //最初の空白を削除

        //それぞれのカラムの内容が書き換わっているか確認し
        //書き換わっていたらその内容をmess_text2に追加していく
        if (!(event.getevent_title().equals(event_title))) {
            mess_text2 = event.getevent_title() + "から\n" + event_title +  "に変更されました。確認をおねがいします。";

        }
        if(!(event.getevent_day().equals(event_day))){
            mess_text2 = mess_text2 + "\n" + event.getevent_day() + "から\n" + event_day +  "に変更されました。確認をおねがいします。\n";

        }
        if (!(event.getevent_place().equals(event_place))) {
            mess_text2 = mess_text2 + "\n" + event.getevent_place() + "から\n" + event_place +  "に変更されました。確認をおねがいします。\n";

        }
        if(!(event.getevent_content().equals(event_content))){
            mess_text2 = mess_text2 + "\n" + event.getevent_content() + "から\n" + event_content +  "に変更されました。確認をおねがいします。\n";

        }
        if(!(event.getevent_reward().equals(event_reward))){
            mess_text2 = mess_text2 + "\n" + event.getevent_reward() + "から\n" + event_reward +  "に変更されました。確認をおねがいします。\n";

        }
        if(!(String.valueOf(event.getevent_number()).equals(event_number))){
            mess_text2 = mess_text2 + "\n" + event.getevent_number() + "から\n" + event_number +  "に変更されました。確認をおねがいします。\n";
            
        }
        if(!(event.getevent_others().equals(event_others))){
            mess_text2 = mess_text2 + "\n" + event.getevent_others() + "から\n" + event_others +  "に変更されました。確認をおねがいします。\n";
        }

        //空白のみで登録されているかどうか
        if (event_title.equals("") && event_day.equals("") && event_place.equals("")
        && event_content.equals("")
        && event_reward.equals("") && event_number.equals("") && event_others.equals("")){
            session.setAttribute("error", "change");
            response.sendRedirect("eventlist.jsp");
            return null;
        }

        //sqlのUPDATE実行
        EventDAO dao = new EventDAO();
        int change = dao.update(event_title, event_day, event_place, event_content, event_reward, event_number, event_others, event.getevent_id());

        if (change == 0) {
            //変更失敗ページに遷移
            return "eventlist.jsp";
        } else {
            //作成たデータをbeanに登録
            List<Event_list> list = new ArrayList<Event_list>();
            Event_listDAO evendao = new Event_listDAO();
            list = evendao.select(event.getevent_id());
            User_message mess = new User_message();
            User_message mess2 = new User_message();
            User_messageDAO messdao = new User_messageDAO();
            mess.setEvent3_id(event.getevent_id());
            mess.setJititai3_id(event.getjititai4_id());
            mess.setUmess_text(mess_text);
            mess2.setEvent3_id(event.getevent_id());
            mess2.setJititai3_id(event.getjititai4_id());
            mess2.setUmess_text(mess_text2);

            //作成したメッセージをDBに登録
            for (int i = 0; i < list.size(); i++) {
                mess.setUserfk_id(list.get(i).getUser_id());
                messdao.insert(mess);
                if (!(mess_text2.equals(""))) {
                    mess2.setUserfk_id(list.get(i).getUser_id());
                    messdao.insert(mess2);
                }
            }

            //変更したイベントに参加しているユーザーのお知らせを未読に変更
            Event_listDAO listdao = new Event_listDAO();
            listdao.read_midoku_change(event.getevent_id(), "未読");

            EventDAO eventdao = new EventDAO();
                List<Event> viewuevent = eventdao.slectall(event.getjititai4_id());
                event = eventdao.select(event.getevent_id());
                if (viewuevent != null) {
                    session.setAttribute("jitiallevent", viewuevent);
                    session.setAttribute("eventdate", event);
                }


            //正常なページ遷移
            response.sendRedirect("eventlist.jsp");
        }
        return "";
    }
}
