package jititai;

import bean.*;
import dao.*;
import tool.Action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.*;
import java.util.Date;

public class EventlistAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {

        Date dNow = new Date( );
        SimpleDateFormat ft = 
        new SimpleDateFormat ("yyyy-MM-dd");
        String today = ft.format(dNow);
        //セッションの開始
        HttpSession session=request.getSession();

        List<Event> event = (List<Event>) session.getAttribute("jitiallevent");
        String in = request.getParameter("index");
        String all = request.getParameter("all");
        if (all.equals("ok")) {
            event = (List<Event>) session.getAttribute("viewuevent");
        }
        int index = Integer.parseInt(in);
        Event eventdate = event.get(index);
        session.setAttribute("eventtoday", today);
        session.setAttribute("eventdate", eventdate);

        //参加フラグが０のユーザーを検索
        Event_listDAO listdao = new Event_listDAO();
        List<Event_list> joinlist = listdao.select(eventdate.getevent_id());
        User user = new User();
        UserDAO userdao = new UserDAO();
        List<User> user_list = new ArrayList<User>();
        int[] join_flag = new int[joinlist.size()];

        for (int i = 0; i < joinlist.size(); i++) {
            user = userdao.select(joinlist.get(i).getUser_id());
            if (user != null) {
                user_list.add(user);
                join_flag[i] = joinlist.get(i).getJoin_flag();
            }
        }

        if (user_list != null) {
            session.setAttribute("user_list", user_list);
            session.setAttribute("event_join_flag", join_flag);
        }
        //正常なページ遷移
        session.setAttribute("report", 0);
        response.sendRedirect("eventlist.jsp");

        return "";
    }
    
}
