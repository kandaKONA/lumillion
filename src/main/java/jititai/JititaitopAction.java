package jititai;

import bean.*;
import dao.*;
import tool.Action;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.*;

public class JititaitopAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        try {
            //セッションの取得
            HttpSession session=request.getSession();

           
            Jititai jititai = (Jititai) session.getAttribute("jititai");
            
            
            if(jititai != null){
                //表示する内容取得
                //ユーザーイベント
                EventDAO eventdao = new EventDAO();
                List<Event> viewuevent = eventdao.slectall(jititai.getJititai_id());
                if (viewuevent != null) {
                    session.setAttribute("jitiallevent", viewuevent);
                }
                //企業イベント
                Event2DAO event2dao = new Event2DAO();
                List<Event2> viewcevent = event2dao.slectall(jititai.getJititai_id());
                if (viewcevent != null) {
                    session.setAttribute("jitiallevent2", viewcevent);
                }
                //やり取りしている企業とのイベント、景品を検索
                Item_history item = new Item_history();
                item.setJititai_id(jititai.getJititai_id());
                //メッセージをやり取りしているイベント一覧
                Item_historyDAO itemdao = new Item_historyDAO();
                List<Item_history> his = itemdao.selectalljiti(item);

                if (his != null) {
                    //自治体名、イベント名、景品名を取得する
                    Company com = new Company();
                    Event2 eve = new Event2();
                    Item ite = new Item();
                    List<Company> comlist = new ArrayList<Company>();
                    List<Event2> evelist = new ArrayList<Event2>();
                    List<Item> itelist = new ArrayList<Item>();
                    CompanyDAO comdao = new CompanyDAO();
                    Event2DAO evedao = new Event2DAO();
                    ItemDAO itedao = new ItemDAO();
                    int count = 0;

                    //既読チェック
                    List<String> jitiread = new ArrayList<String>();

                    for (int i = 0; i < his.size(); i++) {
                        com = comdao.select(his.get(i).getCompany_id());
                        eve = evedao.select(his.get(i).getEvent2_id());
                        ite = itedao.select(his.get(i).getItem_id());
                        jitiread.add(his.get(i).getRead_check());
                    

                        if (com != null && eve != null && ite != null) {
                            comlist.add(com);
                            evelist.add(eve);
                            itelist.add(ite);
                            count++;
                            if (count == 5) {
                                break;
                            }
                        }

                    }
                    //全てのeventの情報をセッションに保存
                    session.setAttribute("comlist5j", comlist);
                    session.setAttribute("evelist5j", evelist);
                    session.setAttribute("itelist5j", itelist);
                    session.setAttribute("size5j", comlist.size());
                    session.setAttribute("jitiread", jitiread);
            
                    //jititaiの情報をセッションに保存
                    session.setAttribute("jititai", jititai);
                    //正常なページ遷移
                    response.sendRedirect("./jititai.jsp");
                }
            }else{
                session.setAttribute("error", "login");
                response.sendRedirect("../../loginjititailogin.jsp");
            }
            return"";
        } catch (Exception e) {
            System.out.println(e);
           return"";
        }
    }
}