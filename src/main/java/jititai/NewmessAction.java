package jititai;

import bean.*;
import dao.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;

public class NewmessAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの開始
        HttpSession session=request.getSession();

        List<Message> allmess = (List<Message>) session.getAttribute("jitiallmess");
        String mess_text = request.getParameter("message").replaceFirst("^[\\h]+", "");  //最初の空白を削除

        //空白のみで登録されているかどうか
        if (mess_text.equals("")) {
            session.setAttribute("error", "mess");
            response.sendRedirect("jititaimess.jsp");
            return null;
        }
        
        int company_id = allmess.get(0).getCompany2_id();
        int event_id = allmess.get(0).getEvent2_id();
        int jititai_id = allmess.get(0).getJititai2_id();

        Message mess = new Message();
        mess.setCompany2_id(company_id);
        mess.setEvent2_id(event_id);
        mess.setJititai2_id(jititai_id);
        mess.setMess_text(mess_text);
        mess.setFlag(1);

        MessageDAO messdao = new MessageDAO();

        int line = messdao.insert(mess);
        if (line == 0) {
            //エラーなページ遷移
            response.sendRedirect("jititaimess.jsp");
            return "";
        }
        //未読変更
        Item_history read = new Item_history();
        read.setCompany_id(company_id);
        read.setJititai_id(jititai_id);
        read.setEvent2_id(event_id);
        read.setRead_check("企業未読");
        Item_historyDAO itemdao = new Item_historyDAO();
        itemdao.change_read(read);
        allmess = messdao.limitslecttalk(mess);

        if (allmess != null) {
            session.setAttribute("jitiallmess", allmess);
            //正常なページ遷移
            response.sendRedirect("jititaimess.jsp");
        }

        return "jititaimess.jsp";
    }
}

    
