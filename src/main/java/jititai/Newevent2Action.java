package jititai;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import java.util.List;

public class Newevent2Action extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        Jititai jiti = (Jititai) session.getAttribute("jititai");
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String event_title = request.getParameter("event_title").replaceFirst("^[\\h]+", "");  //最初の空白を削除
        String event_day = request.getParameter("event_day").replaceFirst("^[\\h]+", "");
        String event_content = request.getParameter("event_content").replaceFirst("^[\\h]+", "");
        String event_place = request.getParameter("event_place").replaceFirst("^[\\h]+", "");
        String pref = request.getParameter("pref");

        if (event_title.equals("") || event_day.equals("") 
        || event_content.equals("") || event_place.equals("")) {
            session.setAttribute("error", "new");
            response.sendRedirect("newevent2.jsp");
            return null;   
        }

        //beanのEvent2に保存
        Event2 event_in = new Event2();
        event_in.setjititai5_id(jiti.getJititai_id());
        event_in.setevent2_title(event_title);
        event_in.setevent2_day(event_day);
        event_in.setevent2_text(event_content);
        event_in.setevent2_place(pref + event_place);

        //Event2DAOを使いDBに追加
        Event2DAO dao = new Event2DAO();
        int insert = dao.insert(event_in);

        if (insert == 0) {
            //登録失敗ページに遷移
            return "";
        } else {
            List<Event2> allevent2 = dao.limitslect(jiti.getJititai_id());
            if(allevent2 != null){
                //全てのeventの情報をセッションに保存
                session.setAttribute("jitiallevent2", allevent2);
                
                //正常なページ遷移
                response.sendRedirect("jititai.jsp");
            }
        }

        return "";
    }
}
