package jititai;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import javax.servlet.*;
import java.util.List;

public class ChangejititaiAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws ServletException,Exception {
        //セッションの確認
        HttpSession session=request.getSession();

        Jititai jititai = (Jititai) session.getAttribute("jititai");
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String jititai_name = request.getParameter("jititai_name");
        String jititai_addres = request.getParameter("jititai_addres");
        String jititai_mailaddres = request.getParameter("jititai_mailaddres");
        jititai_name = jititai_name.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        jititai_addres = jititai_addres.replaceFirst("^[\\h]+", "");  //最初の空白を削除
        jititai_mailaddres = jititai_mailaddres.replaceFirst("^[\\h]+", "");  //最初の空白を削除

        //空白のみで登録されているかどうか
        if (jititai_name.equals("")) {
            session.setAttribute("error", "change");
            response.sendRedirect("changejititai.jsp");
            return null;
        }

        //sqlのUPDATE実行
        JititaiDAO dao = new JititaiDAO();
        int change = dao.update(jititai_name, jititai_addres, jititai_mailaddres, jititai.getJititai_id());

        if (change == 0) {
            //変更失敗ページに遷移
            return "jititai.jsp";
        } else {
            Jititai alljititai = dao.select(jititai.getJititai_id());
            //全てのjititaiの情報をセッションに保存
            session.setAttribute("jititai", alljititai);
            //正常なページ遷移
            response.sendRedirect("jititai.jsp");
        }
        return "";
    }
}
