package jititai;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import java.util.List;

public class NeweventAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        Jititai jiti = (Jititai) session.getAttribute("jititai");
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String event_title = request.getParameter("event_title").replaceFirst("^[\\h]+", "");  //最初の空白を削除
        String event_day = request.getParameter("event_day").replaceFirst("^[\\h]+", "");
        String event_place = request.getParameter("event_place").replaceFirst("^[\\h]+", "");
        String event_content = request.getParameter("event_content").replaceFirst("^[\\h]+", "");
        String event_number = request.getParameter("event_number").replaceFirst("^[\\h]+", "");
        String event_reward = request.getParameter("event_reward").replaceFirst("^[\\h]+", "");
        String event_others = request.getParameter("event_others").replaceFirst("^[\\h]+", "");
        String pref = request.getParameter("pref");

        //空白のみで登録されているかどうか
        if (event_title.equals("") || event_day.equals("") || event_place.equals("")
        || event_content.equals("") || event_number.equals("")) {
            session.setAttribute("error", "new");
            response.sendRedirect("newevent.jsp");
            return null;
        }

        //beanのEventに保存
        Event event_in = new Event();
        event_in.setjititai4_id(jiti.getJititai_id());
        event_in.setevent_title(event_title);
        event_in.setevent_day(event_day);
        event_in.setevent_place(pref + event_place);
        event_in.setevent_content(event_content);
        event_in.setevent_reward(event_reward);
        event_in.setevent_number(Integer.parseInt(event_number));
        event_in.setevent_others(event_others);

        //EventDAOを使いDBに追加
        EventDAO dao = new EventDAO();
        int insert = dao.insert(event_in);

        if (insert == 0) {
            //登録失敗ページに遷移
            return "";
        } else {
            List<Event> allevent = dao.slectall(jiti.getJititai_id());
            if(allevent != null){
                //全てのeventの情報をセッションに保存
                session.setAttribute("jitiallevent", allevent);
                
                //正常なページ遷移
                response.sendRedirect("jititai.jsp");
            }
        }

        return "";
    }
}
