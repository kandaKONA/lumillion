package jititai;

import bean.*;
import dao.*;
import tool.Action;
import java.util.List;
import javax.servlet.http.*;

public class JititaimessAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの開始
        HttpSession session=request.getSession();

        String event2 = request.getParameter("event2_id");
        String com = request.getParameter("company_id");
        Jititai jititai = (Jititai) session.getAttribute("jititai");

        //文字列から数値に変換
        int event2_id = Integer.parseInt(event2);
        int company_id = Integer.parseInt(com);

        Message mess = new Message();
        mess.setCompany2_id(company_id);
        mess.setEvent2_id(event2_id);
        mess.setJititai2_id(jititai.getJititai_id());
        //メッセージ取得
        MessageDAO messdao = new MessageDAO();
        List<Message> allmess = messdao.limitslecttalk(mess);

        //既読かどうか
        Item_history read = new Item_history();
        Item_historyDAO itemdao = new Item_historyDAO();
        read.setCompany_id(company_id);
        read.setEvent2_id(event2_id);
        read.setJititai_id(jititai.getJititai_id());
        read=itemdao.select_read(read);
        if (read.getRead_check().equals("自治体未読")) {
            read.setRead_check("既読");
            itemdao.change_read(read);
        }

        if (allmess != null) {
            session.setAttribute("jitiallmess", allmess);
            //正常なページ遷移
            response.sendRedirect("jititaimess.jsp");
        }

        return "";
    }
    
}
