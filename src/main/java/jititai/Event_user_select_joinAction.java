package jititai;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;

public class Event_user_select_joinAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        HttpSession session=request.getSession();

        Event event = (Event) session.getAttribute("eventdate");
        
        String button = request.getParameter("button");
        String[] user = request.getParameterValues("today_join");
        String[] user_not = request.getParameterValues("today_notjoin");

        Event_listDAO dao = new Event_listDAO();
        
        if (button.equals("join")) {
            
            //参加者人数
            int join_count = 0;
            int notjoin_count = 0;
            if (user != null) {
                for (int i = 0; i < user.length; i++) {
                    join_count = join_count + dao.flagchange(event.getevent_id(), Integer.parseInt(user[i])
                    , 1);
                }
            }
            if (user_not != null) {
                for (int i = 0; i < user_not.length; i++) {
                    notjoin_count = notjoin_count + dao.flagchange(event.getevent_id(), Integer.parseInt(user_not[i])
                    , 0);
                }
            }
            //何人かえたか
            session.setAttribute("count", join_count);
            session.setAttribute("notcount", notjoin_count);

        } else if (button.equals("report")) {
            UserDAO user_dao = new UserDAO();
            int report_count = 0;
            if (user != null) {
                for (int i = 0; i < user.length; i++) {
                    report_count = report_count + dao.flagchange(event.getevent_id(), Integer.parseInt(user[i])
                    , 2);
                    user_dao.ngcount(Integer.parseInt(user[i]));
                }
            }
            if (user_not != null) {
                for (int i = 0; i < user_not.length; i++) {
                    report_count = report_count + dao.flagchange(event.getevent_id(), Integer.parseInt(user_not[i])
                    , 2);
                    user_dao.ngcount(Integer.parseInt(user_not[i]));
                }
            }

        }else if (button.equals("notreport")) {
            UserDAO user_dao = new UserDAO();
            String[] notreport = request.getParameterValues("report_user");
            if (notreport != null) {
                for (int i = 0; i < notreport.length; i++) {
                    user_dao.not_ngcount(Integer.parseInt(notreport[i]));
                }
                dao.notreport(event.getevent_id());
            }
            
        }
        
            
        //正常なページ遷移
        response.sendRedirect("jititai.jsp");

        return "";
    }
}
