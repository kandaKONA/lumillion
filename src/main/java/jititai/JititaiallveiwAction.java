package jititai;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import java.util.ArrayList;
import java.util.List;

public class JititaiallveiwAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();
        Jititai jiti = (Jititai) session.getAttribute("jititai");
        
        String gen = request.getParameter("jitigen");

        //送られてきたgenでどの情報を引き出すか決める
        //ユーザーイベント
        if (gen.equals("uevent")) {
            EventDAO eventdao = new EventDAO();
            List<Event> viewuevent = eventdao.limitslect(jiti.getJititai_id());

            if (viewuevent != null) {

                session.setAttribute("viewuevent", viewuevent);
                session.setAttribute("jitigen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }
        } else if (gen.equals("cevent")) {
            //企業イベント
            Event2DAO eventdao = new Event2DAO();
            List<Event2> viewcevent = eventdao.limitslect(jiti.getJititai_id());

            if (viewcevent != null) {

                session.setAttribute("viewcevent", viewcevent);
                session.setAttribute("jitigen", gen);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            }
        } else if(gen.equals("mess") || gen.equals("history")){
            //自治体idを取得後beanにセット
            Jititai jititai = (Jititai) session.getAttribute("jititai");
            Item_history item = new Item_history();
            item.setJititai_id(jititai.getJititai_id());
            //メッセージをやり取りしているイベント一覧
            Item_historyDAO itemdao = new Item_historyDAO();
            List<Item_history> his = itemdao.selectalljiti(item);

            if (his != null) {
                //自治体名、イベント名、景品名を取得する
                Company com = new Company();
                Event2 eve = new Event2();
                Item ite = new Item();
                List<Company> comlist = new ArrayList<Company>();
                List<Event2> evelist = new ArrayList<Event2>();
                List<Item> itelist = new ArrayList<Item>();
                CompanyDAO comdao = new CompanyDAO();
                Event2DAO evedao = new Event2DAO();
                ItemDAO itedao = new ItemDAO();

                //既読チェック
                List<String> jitiread = new ArrayList<String>();

                for (int i = 0; i < his.size(); i++) {
                    com = comdao.select(his.get(i).getCompany_id());
                    eve = evedao.select(his.get(i).getEvent2_id());
                    ite = itedao.select(his.get(i).getItem_id());
                    jitiread.add(his.get(i).getRead_check());
                    

                    if (com != null && eve != null && ite != null) {
                        comlist.add(com);
                        evelist.add(eve);
                        itelist.add(ite);
                    }

                }
                //全てのeventの情報をセッションに保存
                session.setAttribute("comlist", comlist);
                session.setAttribute("evelist", evelist);
                session.setAttribute("itelist", itelist);
                session.setAttribute("size", comlist.size());
                session.setAttribute("jitigen", gen);
                session.setAttribute("alljitiread", jitiread);
                //正常なページ遷移
                response.sendRedirect("allview.jsp");
            
            }
        }
        
        return "";
    }
    
}
