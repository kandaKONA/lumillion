package jititai;

import bean.*;
import dao.*;
import tool.Action;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.*;

public class Eventlist2Action extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの開始
        HttpSession session=request.getSession();

        //イベント内容の表示
        List<Event2> event = (List<Event2>) session.getAttribute("jitiallevent2");
        String in = request.getParameter("index");
        int index = Integer.parseInt(in);
        if (index > 4) {
            event = (List<Event2>) session.getAttribute("viewcevent");
        }
        Event2 eventdate = event.get(index);

        //イベントに対して景品が送られているかどうか
        Item_historyDAO itemdao = new Item_historyDAO();
        List<Item_history> his = itemdao.selectallevent(event.get(index).
        getevent2_id());
        
        if (his != null) {
            //自治体名、景品名を取得する
            Company com = new Company();
            Item ite = new Item();
            List<Company> comlist = new ArrayList<Company>();
            List<Item> itelist = new ArrayList<Item>();
            CompanyDAO comdao = new CompanyDAO();
            ItemDAO itedao = new ItemDAO();
            //既読チェック
            List<String> jitiread = new ArrayList<String>();

            for (int i = 0; i < his.size(); i++) {
                com = comdao.select(his.get(i).getCompany_id());
                ite = itedao.select(his.get(i).getItem_id());
                jitiread.add(his.get(i).getRead_check());
                

                if (com != null && ite != null) {
                    comlist.add(com);
                    itelist.add(ite);
                }

            }
            //全てのeventの情報をセッションに保存
            session.setAttribute("ecomlist", comlist);
            session.setAttribute("eitelist", itelist);
            session.setAttribute("eventsize", comlist.size());
            session.setAttribute("jitiread", jitiread);
        }
        
        session.setAttribute("event2date", eventdate);

        //正常なページ遷移
        response.sendRedirect("eventlist2.jsp");

        return "";
    }    
}
