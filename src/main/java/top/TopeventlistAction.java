package top;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;

public class TopeventlistAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        HttpSession session=request.getSession();
        
        String id = request.getParameter("event_id");
        int event_id = Integer.parseInt(id);
        EventDAO dao = new EventDAO();
        Event event = dao.select(event_id);
        if(event != null){
            //1つのeventの情報をセッションに保存
            session.setAttribute("event", event);
            
            //正常なページ遷移
            response.sendRedirect("topevent.jsp");
        }

        return "";
    }
}