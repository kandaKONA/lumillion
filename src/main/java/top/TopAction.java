package top;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;
import java.util.List;

public class TopAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        HttpSession session=request.getSession();

        EventDAO dao = new EventDAO();
        List<Event> allevent = dao.slectall();
        if(allevent != null){
            //全てのeventの情報をセッションに保存
            session.setAttribute("allevent", allevent);
            session.setAttribute("error", null);
            //正常なページ遷移
            response.sendRedirect("top.jsp");
        }else{
            response.sendRedirect("error.jsp");
        }

        return "";
    }
}
