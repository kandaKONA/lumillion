package message;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;

import java.util.List;

public class NewmessAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        
        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String event2_id = request.getParameter("event2_id");
        String company2_id = request.getParameter("company2_id");
        String jititai2_id = request.getParameter("jititai2_id");
        String mess_text = request.getParameter("mess_text");
        String flag = request.getParameter("flag");
        
        
        //beanのMessageに保存
        Message mess_in = new Message();
        mess_in.setEvent2_id(Integer.parseInt(event2_id));
        mess_in.setCompany2_id(Integer.parseInt(company2_id));
        mess_in.setJititai2_id(Integer.parseInt(jititai2_id));
        mess_in.setMess_text(mess_text);
        mess_in.setFlag(Integer.parseInt(flag));

        MessageDAO dao = new MessageDAO();
        int insert = dao.insert(mess_in);

        if (insert == 0) {
            //登録失敗ページに遷移
            return "";
        } else {
            List<Message> message = dao.slectall();
            if(message != null){
                //全てのeventの情報をセッションに保存
                session.setAttribute("message", message);
                
                //正常なページ遷移
                response.sendRedirect("message.jsp");
            }
        }

        return "";
    }
}
