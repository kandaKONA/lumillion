package message;

import bean.*;
import dao.*;
import tool.Action;
import javax.servlet.http.*;

import java.util.List;

public class NewusermeesAction extends Action{
    public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {
        //セッションの取得
        HttpSession session=request.getSession();

        //jspのname="　　"を参照してそこに入力された情報を変数に保存
        String event3_id = request.getParameter("event3_id");
        String userfk_id = request.getParameter("userfk_id");
        String jititai3_id = request.getParameter("jititai3_id");
        String umess_text = request.getParameter("umess_text");
        
        //beanのEventに保存
        User_message umess_in = new User_message();
        umess_in.setEvent3_id(Integer.parseInt(event3_id));
        umess_in.setUserfk_id(Integer.parseInt(userfk_id));
        umess_in.setJititai3_id(Integer.parseInt(jititai3_id));
        umess_in.setUmess_text(umess_text);

        User_messageDAO dao = new User_messageDAO();
        int insert = dao.insert(umess_in);

        if (insert == 0) {
            //登録失敗ページに遷移
            return "";
        } else {
            List<User_message> user_message = dao.slecttalk(umess_in);
            if(user_message != null){
                //全てのeventの情報をセッションに保存
                session.setAttribute("user_message", user_message);
                
                //正常なページ遷移
                response.sendRedirect("jititai.jsp");
            }
        }

        return "";
    }
}
