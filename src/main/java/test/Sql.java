package test;

import dao.DAO;
import java.sql.*;

public class Sql {
    public static void main(String[] args) {
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        DAO dao = new DAO();
    
    
        try {
          con= dao.getConnection();
          // "password"の部分は，各自の環境に合わせて変更してください。
    
          pstmt = con.prepareStatement("select * from event");
    
          rs = pstmt.executeQuery();
    
          while (rs.next()) {
            System.out.println(rs.getString("event_id"));
            System.out.println(rs.getInt("event_name"));
          }
    
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          if (rs != null) {
            try {
              rs.close();
            } catch (SQLException e) {
              e.printStackTrace();
            }
          }
          if (pstmt != null) {
            try {
              pstmt.close();
            } catch (SQLException e) {
              e.printStackTrace();
            }
          }
          if (con != null) {
            try {
              con.close();
            } catch (SQLException e) {
              e.printStackTrace();
            }
          }
        }
      }
}
