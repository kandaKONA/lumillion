<%@include file="../header.jsp" %>
<%-- 画面3 --%>
<!doctype html>
<html>

<head>
   <meta charset="utf-8"/>
   <title>企業のメイン画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="company_btn.css">
    <link rel="stylesheet" href="../CSS/box.css">
</head>

<body>
    <%@include file="company_btn.jsp" %>

       <h2>${company.company_name}</h2>
    
       <details class="content" open>
        <summary>イベント一覧</summary>

        <%-- 検索ボックス領域 --%>
        <%@include file="../search/search.jsp" %>

        <c:choose>
            <c:when test="${comallevent2.size()!=0}">
                <c:forEach var="event2" items="${comallevent2}">
                    <p>【日程${event2.event2_day}】
                     <a href="Eventitem.action?event2_id=${event2.event2_id}">
                         ${event2.event2_title}</p></a>
                </c:forEach>
            </c:when>
            <c:when test="${comallevent2.size()==0}">
                <p>検索結果がありません</p>
            </c:when>
        </c:choose>
        <div class="right">
            <p class="link">
                <a href="Companyallveiw.action?comgen=event2">もっと見る</a></p>
        </div>

    </details>

    <details class="content">
        <summary>メッセージ一覧</summary>

        <c:choose>
                <c:when test="${evelist5c.size()!=0}">
                    <c:forEach begin="0" end="${size5c}" varStatus="i">
                        <c:choose>
                            <c:when test="${comread[i.index]=='企業未読'}">
                                <p>
                                    <a href="Companymess.action?event2_id=${evelist5c[i.index].event2_id}&jititai_id=${jitilist5c[i.index].jititai_id}">
                                        ${evelist5c[i.index].event2_title} ${jitilist5c[i.index].jititai_name}
                                        ${itelist5c[i.index].item_name}
                                    </a>
                                    未読あり
                                </p>
                            </c:when>
                            <c:when test="${comread[i.index]!='企業未読'}">
                                <a href="Companymess.action?event2_id=${evelist5c[i.index].event2_id}&jititai_id=${jitilist5c[i.index].jititai_id}">
                                    <p>${evelist5c[i.index].event2_title} ${jitilist5c[i.index].jititai_name}
                                    ${itelist5c[i.index].item_name}
                                    </p>
                                </a>
                            </c:when>
                        </c:choose>
                    </c:forEach>
                </c:when>
                <c:when test="${evelist5c.size()==0}">
                  <p>メッセージがありません</p>
                </c:when>
        </c:choose>

        <div class="right">
            <p class="link">
              <a href="Companyallveiw.action?comgen=mess">もっと見る</a></p>
        </div>
    </details>

        <details class="content">
            <summary>交換履歴</summary>

            <c:choose>
                <c:when test="${evelist5c.size()!=0}">
                    <c:forEach begin="0" end="${size5c}" varStatus="i">
                        <p>${jitilist5c[i.index].jititai_name}
                            ${itelist5c[i.index].item_name}
                        </p>
                    </c:forEach>
                </c:when>
                <c:when test="${evelist5c.size()==0}">
                    <p>交換履歴がありません</p>
                </c:when>
            </c:choose>

            <div class="right">
                <p class="link">
                  <a href="Companyallveiw.action?comgen=history">もっと見る</a></p>
            </div>

        </details>
    
</body>