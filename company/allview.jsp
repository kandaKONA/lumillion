<%@include file="../header.jsp" %>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>企業の一覧画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="company_btn.css">
    <link rel="stylesheet" href="../CSS/box.css">
    <link rel="stylesheet" href="../CSS/allview.css">
    
    
</head>
<body>
  <%-- 共有ボタンの設置 --%>
    <%@include file="company_btn.jsp"%>

    <h2>${company.company_name}</h2>

    <div class="content">
        <c:choose>
            <c:when test="${comgen=='event2'}">
                <p class="alltitle">イベント一覧</p>
                <%-- 検索ボックス領域 --%>
                <%@include file="../search/allsearch.jsp" %>
                <div class="view">
                <c:choose>
                    <c:when test="${viewevent2.size()!=0}">
                        <c:forEach var="item" items="${viewevent2}">
                            <p>【日程${item.event2_day}】
                            <a href="Eventitem.action?event2_id=${item.event2_id}">
                                ${item.event2_title}
                            </a></p>
                        </c:forEach>
                    </c:when>
                    <c:when test="${viewevent2.size()==0}">
                        <p>検索結果がありません</p>
                    </c:when>
                </c:choose>
            </c:when>
            <c:when test="${comgen=='mess'}">
                <p class="alltitle">メッセージ一覧</p>
                <div class="view">
                <c:forEach begin="0" end="${size}" varStatus="i">
                    <c:choose>
                        <c:when test="${allcomread[i.index]=='企業未読'}">
                            <p>
                                <a href="Companymess.action?event2_id=${evelist[i.index].event2_id}&jititai_id=${jitilist[i.index].jititai_id}">
                                    ${evelist[i.index].event2_title} ${jitilist[i.index].jititai_name}
                                     ${itelist[i.index].item_name}
                                </a>
                                未読あり
                            </p>
                        </c:when>
                        <c:when test="${allcomread[i.index]!='企業未読'}">
                            <p>
                                <a href="Companymess.action?event2_id=${evelist[i.index].event2_id}&jititai_id=${jitilist[i.index].jititai_id}">
                                    ${evelist[i.index].event2_title} ${jitilist[i.index].jititai_name}
                                     ${itelist[i.index].item_name}
                                </a>
                            </p>
                        </c:when>
                    </c:choose>
                    
                </c:forEach>
            </c:when>
            <c:when test="${comgen=='history'}">
                <p class="alltitle">交換履歴一覧</p>
                <div class="view">
                <c:forEach begin="0" end="${size}" varStatus="i">
                        <p>${evelist[i.index].event2_title} ${jitilist[i.index].jititai_name}
                             ${itelist[i.index].item_name}
                        </p>
                </c:forEach>
            </c:when>
            <c:when test="${comgen=='item'}">
                <p class="alltitle">景品一覧</p>
                <div class="view">
                <c:forEach var="item" items="${allitem}">
                    <a href="Itemselect.action?item_id=${item.item_id}">
                        <p>${item.item_name}</p>
                    </a>
                </c:forEach>
            </c:when>
        </c:choose>
    </div>
    </div>
    <script src="../JS/jquery-3.3.1.min.js"></script>
    <script src="../JS/paginathing.min.js"></script>
    <script src="../JS/user/page.js"></script>
</body>
</html>