<%@include file="../header.jsp" %>
<%-- 画面７ --%>
<html>
<head>
   <meta charset="utf-8"/>
   <title>景品登録画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="gamen7.css">
    <link rel="stylesheet" href="company_btn.css">
</head>
<body>
    <%-- 共有ボタンの設置 --%>
    <%@include file="company_btn.jsp"%>

    <%-- エラー表示 --%>
    <%@include file="../error/error.jsp" %>
   
       <h2>景品登録</h2>
    </header>
    <form action="Newitem.action" method="post" enctype="multipart/form-data">
        <div class="content">

            <text>景品の写真</text><br>
            <input type="file" name="image" accept="image/*" id="img"><br><br>
           <img style="width: 700px; height: 500px"
           id="imgview" alt="選択された画像が表示されます"><br>

            <text>景品名</text><br>
            <textarea class="tel" rows="2" cols="35" name="item_name" required></textarea><br>

            <text>コメント</text><br>
            <textarea class="tel" rows="6" cols="35"font-size= "1.8em" name="item_text"></textarea><br>

        </div>
        <p class="btn1">
            <button type="submit">登録</button></p>
    </form>
        
        <script src="../JS/jquery-3.3.1.min.js"></script>
        
        <script>

            $("#img").on("change", function (e) {

                // 2. 画像ファイルの読み込みクラス
                var reader = new FileReader();

                // 3. 準備が終わったら、id=sample1のsrc属性に選択した画像ファイルの情報を設定
                reader.onload = function (e) {
                    $("#imgview").attr("src", e.target.result);
                }

            // 4. 読み込んだ画像ファイルをURLに変換
            reader.readAsDataURL(e.target.files[0]);
            });
        </script>
        
</body>