<%@include file="../header.jsp" %>
<%-- 画面８ --%>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>景品詳細画面</title>
   <link rel="stylesheet" href="../CSS/master.css">
   <link rel="stylesheet" href="company_btn.css">
   <link rel="stylesheet" href="../CSS/btn1.css">
   <link rel="stylesheet" href="gamen7.css">
    
</head>
<body>
    <%-- 共有ボタンの設置 --%>
    <%@include file="company_btn.jsp"%>

    <%-- エラー表示 --%>
    <%@include file="../error/error.jsp" %>
   
    <h2>景品詳細</h2>    
    
    
        <div class="content">
            <form action="Changeitem.action" method="post" enctype="multipart/form-data">
                <text>景品の写真</text><br>
                <input type="file" name="image" accept="image/*" id="img"><br><br>
               <img id="imgview" src="./img?item_id=${item.item_id}" 
               style="width: 700px; height: 500px" alt="選択された写真が表示されます"><br>
    
                <text>景品名</text><br>
                <textarea class="tel" rows="2" cols="35" name="item_name" required>${item.item_name}</textarea><br>
    
                <text>コメント</text><br>
                <textarea class="tel" rows="6" cols="35"font-size= "1.8em" name="item_text">${item.item_text}</textarea><br>
    
            </div>
            <p class="btn1">
                <button class="btnright" type="submit">変更</button></p>
            </form>
   

    <script src="../JS/jquery-3.3.1.min.js"></script>
    <script src="../JS/company/image.js"></script>
</body>