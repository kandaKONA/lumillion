<%@include file="../header.jsp" %>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>イベント詳細画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="gamen10.css">
    <link rel="stylesheet" href="company_btn.css">
</head>
<body>
    <%@include file="company_btn.jsp" %>
       <h2>イベント詳細</h2>
    </header>
    <div class="content">
        <text>イベント名</text>  
        <p>${selectevent2.event2_title}</p>
        <text>イベント日時</text>  
        <p>${selectevent2.event2_day}</p>
        <text>内容</text>  
        <p>${selectevent2.event2_text}</p>
        
    </div>
    
    
    <%-- エラー表示 --%>
    <%@include file="../error/error.jsp" %>
    
    <div class="content">
        <p>景品選択</p>
        <select name="" id="select_item" onChange="location.href=value;" required>
            <option value="" hidden>選択してください</option>
            <c:forEach var="item" items="${formitem}">
                <option value="Returnitem.action?item_id=${item.item_id}">
                    ${item.item_name}
                </option>
                
            </c:forEach>
        </select> <br>

        <text>景品名</text>
        <p>${reitem.item_name}</p>
        <p>景品写真</p>
        <img id="imgview" src="./img?item_id=${reitem.item_id}" 
               style="width: 700px; height: 450px" alt="登録された写真が表示されます"> 
               <br>
        <text>景品説明</text>
        <p>${reitem.item_text}</p>
    </div>
    <form action="Newitemhistory.action">
        <div class="left">
        <p class="btn1">
            <button type="submit">送る</button></p>
        </div>
    </form>

    <script src="../JS/jquery-3.3.1.min.js"></script>
    <script src="../JS/company/image.js"></script>
</body>