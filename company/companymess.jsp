<%@include file="../header.jsp" %>
<!doctype html>
<html>

<head>

   <meta charset="utf-8"/>

   <title>メッセージ一覧</title>

   <link rel="stylesheet" href="../CSS/mess.css">
   <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="company_btn.css">
</head>

<body>

</div>
<%@include file="company_btn.jsp" %>
       <h2>メッセージ画面</h2>

    </header>

    <div class="content1">
        <p class="title">メッセージ一覧</p><br>

        <div class="mess" id="scrol">
            
            <c:forEach var="mess" items="${comallmess}">
                <div class="line-bc"><!--①LINE会話全体を囲う-->

                <c:if test="${mess.flag==0}">

                    <!--③右コメント始-->
                <div class="mycomment">
                    <!--右ふきだし文-->
                    <pre>${mess.mess_text}</pre>
                  </div>
                  <!--/③右コメント終-->

                </c:if>
                <c:if test="${mess.flag==1}">
                    <!--②左コメント始-->
                <div class="balloon6">
                    <div class="chatting">
                      <div class="says">
                        <pre>${mess.mess_text}</pre>
                      </div>
                    </div>
                  </div>
                  <!--②/左コメント終-->

                </c:if>
            </div><!--/①LINE会話終了-->
            </c:forEach>
            
            </div>
    </div>
    <%-- エラー表示 --%>
    <%@include file="../error/error.jsp" %>
    
    <form action="Newmess.action" method="post">
    <div class="col-xs-6 wow animated slideInRight" data-wow-delay=".5s">
        <!-- Message -->
        <textarea name="message" id="message" rows="5" cols="80" class="form textarea"  placeholder=""
        required></textarea>
    </div><!-- End Right Inputs -->

    <p class="btn1">
        <button type="submit">送信</button>
    </p>
    </form>

    <script src="../JS/item/scrol.js"></script>
</body>