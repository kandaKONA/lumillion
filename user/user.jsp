<%@include file="../header.jsp" %>
<html>
<head>
   <meta charset="utf-8"/>
   <title>ユーザーのメイン画面</title>
    
    <link rel="stylesheet" href="../CSS/master.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="user_btn.css">
    <link rel="stylesheet" href="../CSS/box.css">
</head>
<body>
  <c:if test="${error != 'null'}" >
        <%@include file="../error/error.jsp" %>
  </c:if>
  <%-- 共有ボタンの設置 --%>
    <%@include file="user_btn.jsp"%>
       <h2>${user.user_name}</h2>
    </header>
    <%@include file="../error/error.jsp" %>

    <details class="content" open>
      <summary>イベント一覧</summary>
      <%-- 検索ボックス領域 --%>
      <%@include file="../search/search.jsp" %>

      <c:choose>
        <c:when test="${allevent.size()!=0}">
          <c:forEach var="event" items="${allevent}">
            <p>【日程${event.event_day}】
              <a href="Eventlist.action?event_id=${event.event_id}">${event.event_title}</p></a>
          </c:forEach>
        </c:when>
        <c:when test="${allevent.size()==0}">
          <p>検索結果がありません</p>
        </c:when>
      </c:choose>
        
    
      <div class="right">
        <p class="link">
          <a href="Allview.action?gen=event">もっと見る</a></p>
      </div>
    </details>

    <details class="content">
      <summary>イベント参加予定一覧</summary>

        <c:choose>
        <c:when test="${alljoin.size()!=0}">
          <c:forEach var="join" items="${alljoin}">
            <p>【日程${join.event_day}】
          <a href="Event.action?event_id=${join.event_id}">${join.event_title}</p></a>
        </c:forEach>
        </c:when>
        <c:when test="${alljoin.size()==0}">
          <p>参加予定がありません</p>
        </c:when>
      </c:choose>
    
      <div class="right">
        <p class="link">
          <a href="Allview.action?gen=join">もっと見る</a></p>
      </div>
    </details>
    
      
    <details class="content">
      <summary>参加イベントお知らせ</summary>

      <c:choose>
        <c:when test="${allmess.size()!=0}">
          <c:forEach var="mess" items="${allmess}" begin="0" varStatus="i">
            <c:if test="${allmessread[i.index] == '既読'}">
              <a href="Usermess.action?event_id=${mess.event_id}"><p>${mess.event_title}</p></a>
            </c:if>

            <c:if test="${allmessread[i.index] == '未読'}">
              <a href="Usermess.action?event_id=${mess.event_id}"><p>${mess.event_title}</a>
              未読あり</p>
            </c:if>
          </c:forEach>
        </c:when>
        <c:when test="${allmess.size()==0}">
          <p>メッセージがありません</p>
        </c:when>
      </c:choose>

    </div>
    <div class="right">
      <p class="link">
        <a href="Allview.action?gen=mess">もっと見る</a></p>
    </div>
  </details>
    
  </body>
</html>