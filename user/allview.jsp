<%@include file="../header.jsp" %>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>ユーザーのメイン画面</title>
    <link rel="stylesheet" href="../CSS/master.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="user_btn.css">
    <link rel="stylesheet" href="../CSS/box.css">
    <link rel="stylesheet" href="../CSS/allview.css">
</head>
<body>
  <%-- 共有ボタンの設置 --%>
    <%@include file="user_btn.jsp"%>
    <h2>${user.user_name}</h2>
    <div class="content">
        
        <c:choose>
            <c:when test="${gen=='event'}">
                <p class="alltitle">イベント一覧</p>
                <%-- 検索ボックス領域 --%>
                <%@include file="../search/allsearch.jsp" %>
                <div class="view">
                    <c:choose>
                        <c:when test="${viewevent.size()!=0}">
                            <c:forEach var="item" items="${viewevent}">
                                <p>【日程${item.event_day}】
                                <a href="./Eventlist.action?event_id=${item.event_id}">
                                    ${item.event_title}
                                </a></p>
                            </c:forEach>
                        </c:when>
                      <c:when test="${viewevent.size()==0}">
                        <p>検索結果がありません</p>
                      </c:when>
                    </c:choose>
                </div>
            </c:when>
            <c:when test="${gen=='join'}">
                <p class="alltitle">イベント参加予定一覧</p>
                <div class="view">
                    <c:forEach var="item" items="${viewjoin}">
                        <p>【日程${item.event_day}】
                        <a href="Event.action?event_id=${item.event_id}">
                            ${item.event_title}</p></a>
                    </c:forEach>
                </div>
            </c:when>
            <c:when test="${gen=='mess'}">
                <p class="alltitle">参加イベントお知らせ</p>
                    <div class="view">
                    <c:forEach var="item" items="${viewmess}">
                        <a href="Usermess.action?event_id=${item.event_id}">
                            <p>${item.event_title}</p></a>
                    </c:forEach>
                </div>
            </c:when>
            <c:when test="${gen=='joined'}">

                <div class="view">
                    <c:forEach var="item" items="${viewjoined}">
                        <a href="Event.action?event_id=${item.event_id}">
                            <p>${item.event_title}</p></a>
                    </c:forEach>
                </div>
            </c:when>
        </c:choose>
        
    </div>
    <script src="../JS/jquery-3.3.1.min.js"></script>
    <script src="../JS/paginathing.min.js"></script>
    <script src="../JS/user/page.js"></script>
</body>
</html>