<%@include file="../header.jsp" %>
<%-- 画面14 --%>
<html>

<head>

   <meta charset="utf-8"/>

   <title>メッセージ一覧</title>

    <link rel="stylesheet" href="gamen14.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="../CSS/mess.css">
    <link rel="stylesheet" href="user_btn.css">
    

</head>

<body>
    <%-- 共有ボタンの設置 --%>
    <%@include file="user_btn.jsp"%>
    
       <h2>メッセージ画面</h2>

    </header>

    <div class="content1">

        <p class="title">メッセージ一覧</p><br>

        <div class="mess" id="scrol">
        <c:forEach var="mess" items="${user_message}">
            <div class="line-bc"><!--①LINE会話全体を囲う-->

                <!--②左コメント始-->
                <div class="balloon6">
                  <div class="chatting">
                    <div class="says">
                      <pre>${mess.umess_text}</pre>
                    </div>
                  </div>
                </div>
                <!--②/左コメント終-->
              
                <!--③右コメント始-->
                <div class="mycomment">
                  <p>
                  <!--右ふきだし文-->
                  </p>
                </div>
                <!--/③右コメント終-->
              </div><!--/①LINE会話終了-->
        </c:forEach>
        </div>

    </div>

       <script src="../JS/item/scrol.js"></script>

</body>