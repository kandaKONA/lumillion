<%@include file="../header.jsp" %>
<%-- 画面２ --%>
<!doctype html>
<html>
<head>
   <meta charset="utf-8"/>
   <title>イベント一覧</title>
    <link rel="stylesheet" href="gamen2.css">

    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="user_btn.css">
</head>
<body>
    <%-- 共有ボタンの設置 --%>
    <%@include file="user_btn.jsp"%>

    <%-- ユーザー画面でイベントのもっと見るを押したときに出る画面 --%>
       <h2>${user.user_name}</h2>
    </header>
    <div class="content">
        <p>イベント一覧</p>
        
        <%-- リスト化したときはforEachを使う --%>
        <c:forEach var="event" items="${allevent}">
          <a href="./Eventlist.action?event_id=${event.event_id}"><p>${event.event_title}</p></a>
        </c:forEach>
          
        
    </div>
        
</body>