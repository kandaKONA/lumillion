<%@include file="../header.jsp" %>
<!doctype html>
<html>
<head>

   <meta charset="utf-8"/>

   <title>イベント詳細通知画面</title>
    <link rel="stylesheet" href="gamen13.css">
    <link rel="stylesheet" href="../CSS/master.css">
    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="user_btn.css">

</head>

<body>

</div>
<%-- 共有ボタンの設置 --%>
    <%@include file="user_btn.jsp"%>

       <h2>イベント詳細</h2>

    </header>

    <div class="content">

        <text>イベントタイトル</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_title}
        </div>

        <text>イベント内容</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_content}
        </div>

        <text>イベント日時</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_day}
        </div>

        <text>場所</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_place}
        </div>

        <text>参加景品</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_reward}
        </div>

        <text>その他</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${eventdate.event_others}
        </div>
    </div>

    <form action="Eventdelete.action" method="post">
        <p class="btn1">
            <button type="submit">キャンセル</button>
        </p>
    </form>

</body>