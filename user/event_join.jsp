<%@include file="../header.jsp" %>
<%-- 画面13 --%>
<html>
<head>
   <meta charset="utf-8"/>
   <title>イベント詳細(3)</title>
    <link rel="stylesheet" href="gamen13.css">
    <link rel="stylesheet" href="../CSS/master.css">

    <%-- 共有ボタン用のCSS --%>
    <link rel="stylesheet" href="user_btn.css">
</head>
<body>

    <%-- 共有ボタンの設置 --%>
    <%@include file="user_btn.jsp"%>

    <%@include file="../error/error.jsp" %>
       <h2>イベント詳細</h2>
    </header>
    <div class="content">
        <text>イベントタイトル</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_title}
        </div>

        <text>イベント内容</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_content}
        </div>

        <text>参加募集人数</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_number}
        </div>

        <text>イベント日時</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_day}
        </div>

        <text>場所</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_place}
        </div>

        <text>参加景品</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_reward}
        </div>

        <text>その他</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_others}
        </div>
    </div>
    <form action="Eventjoin.action" method="post">
        <p class="btn1">
            <button type="submit">参加</button>
        </p>
    </form>
    
    <form action="user.jsp">
        <p class="btn2">
            <button type="submit">戻る</button>
        </p>
    </form>
</body>