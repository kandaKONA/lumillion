
<%@include file="../header.jsp" %>

<div class="margin_head">
    <!-- 項目を入力してセッションを始める
        複数のページにまたがって扱いたい情報（名前やIDなど）
        を記録する機能のこと
    -->
  <form action="Login.action" method="post">
    <p>ログイン名<input type="text" name="login" autocomplete="off"></p>
    <p>パスワード<input type="password" name="password"></p>
    <p><input type="submit" value="ログイン"></p>
  </form>
  <p><a href="Logout.action">ログアウト</a></p>
    <%-- セッションが生成されてたら表示される --%>
    <c:choose>
      <c:when test="${login != null}">
          <p>名前は${login}です</p>
      </c:when>
      <c:when test="${user != null}">
          <p>名前は${user.user_name}です</p>
      </c:when>
    </c:choose>
</div>

<%@include file="../footer.jsp" %>