<%@include file="../header.jsp" %>

<div class="margin_head">

    <%-- c:chooseはswith文whenで場合分けする --%>
    <c:choose>
        <c:when  test="${login != ''}">
            <p>名前は${login}です</p>
            <p>パスは${password}です</p>
        </c:when>
        <c:when  test="${login == ''}">
            <p>名前を入力してください</p>
        </c:when>
    </c:choose>
    <a href="testlogin.jsp">戻る</a>
</div>

<%@include file="../footer.jsp" %>