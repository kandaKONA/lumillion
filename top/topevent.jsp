<%@include file="../header.jsp" %>
<%-- 画面13 --%>
<html>
<head>
   <meta charset="utf-8"/>
   <title>イベント詳細(3)</title>
   
    <link rel="stylesheet" href="../CSS/master.css">
    <link rel="stylesheet" href="btn.css">
    <link rel="stylesheet" href="../CSS/box.css">

</head>
<body>
    <div>
        <p class="menubtn1">
            
          <button type="submit" onclick="location.href='../newin/newuser.jsp'">新規登録</button></p>
    
        <p class="menubtn2">
          <button type="submit" onclick="location.href='../login/userlogin.jsp'">ログイン</button></p>
        </div>
    
       <h2>イベント詳細</h2>
    </header>
    <div class="content">
        <text>イベントタイトル</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_title}
        </div>

        <text>イベント内容</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_content}
        </div>

        <text>参加募集人数</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_number}
        </div>

        <text>イベント日時</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_day}
        </div>

        <text>場所</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_place}
        </div>

        <text>参加景品</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_reward}
        </div>

        <text>その他</text>
        <div style="padding: 10px; margin-bottom: 10px; border: 5px double #333333; background-color: #66FFCC;">
            ${event.event_others}
        </div>
    </div>
    <div class="box">
        <p class="btn1">
            <button id="notlogin" type="submit">参加</button>
        </p>
        <div class="hidden_box" id="hidden">
            <%-- 非表示要素 --%>
            <button id="outhidden" onclick="">×</button>
            <h3>イベント参加するには新規登録又はログインしてください</h3>
            <p class="btn1">
                <button type="submit" onclick="location.href='../newin/newuser.jsp'">新規登録</button>

            </p>
            <p class="btn1">
                <button type="submit" onclick="location.href='../login/userlogin.jsp'">ログイン</button>

            </p>

        </div>
    </div>
    
    <form action="top.jsp">
        <p class="btn2">
            <button type="submit">戻る</button>
        </p>
    </form>

    <script src="../JS/jquery-3.3.1.min.js"></script>
    <script src="../JS/top/hidden.js"></script>
</body>